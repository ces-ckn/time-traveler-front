'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:ReportCtrl
 * @description
 * # ReportCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

app.controller('ReportCtrl', [
    '$scope', '$http', 'localStorageService', '$state', 'getDataServices',
    function ($scope, $http, localStorageService, $state, getDataServices) {

	var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
    // var apiUrl = 'http://192.168.1.104:3000/api/';	// Tri
    // var apiUrl = 'http://192.168.1.126:3000/api/';	// Thuan

    var shortMonth = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var taskTimeData = [
        { "month": "Jan", "duration": 0 }, 
        { "month": "Feb", "duration": 0 }, 
        { "month": "Mar", "duration": 0 }, 
        { "month": "Apr", "duration": 0 }, 
        { "month": "May", "duration": 0 }, 
        { "month": "Jun", "duration": 0 }, 
        { "month": "Jul", "duration": 0 }, 
        { "month": "Aug", "duration": 0 }, 
        { "month": "Sep", "duration": 0 }, 
        { "month": "Oct", "duration": 0 }, 
        { "month": "Nov", "duration": 0 }, 
        { "month": "Dec", "duration": 0 }
    ];

    // task time chart
    $scope.taskTimeLabels = shortMonth;
    $scope.taskTimeData = [[0,0,0,0,0,0,0,0,0,0,0,0]];
    $scope.taskTime = {};
    $scope.taskTime.unit = 'hour';
    $scope.taskTime.groupBy = 'month';  

    // member's task hour
    // $scope.memberLabels = ["Leliana@dragonage.origin", "Chunli@street.fighter", "Ysolda@tes.skyrim", "Triss@witcher.temeria", "Yenefer@witcher.temeria", "Ciri@witcher.temeria", "Morrigan@dragonage.origin", "Lydia@tes.skyrim"];
    // $scope.memberData = [300, 500, 350, 400, 200, 420, 470, 390];

    // ================================= Filter Region ========================================= //
    $scope.reportData               = {};
    $scope.reportData.companyModel  = {};
    $scope.reportData.teamModel     = {};
    $scope.reportData.projectModel  = {};
    // $scope.reportData.clientModel   = {};

    $scope.companyText  = { buttonDefaultText: 'Company' };
    $scope.teamText     = { buttonDefaultText: 'Team' };
    $scope.projectText  = { buttonDefaultText: 'Project' };
    // $scope.clientText   = { buttonDefaultText: 'Client' };

    $scope.companySettings = {
        enableSearch: true,
        scrollable: true,
        displayProp: 'label', 
        idProp: 'id', 
        externalIdProp: '',
        selectionLimit: 1,
        smartButtonMaxItems: 1,
        groupByTextProvider: function(groupValue) {
            if (groupValue === 'O') {
                return 'Owned Company';
            } else {
                return 'Joined Company';
            }
        }
    };
    $scope.teamSettings = {
        enableSearch: true,
        scrollable: true,
        displayProp: 'label', 
        idProp: 'id', 
        externalIdProp: '', // user_id
        selectionLimit: 1,
        smartButtonMaxItems: 1,
        groupByTextProvider: function(groupValue) {
            if (groupValue === 'O') {
                return 'Owned Members';
            } else {
                return 'Joined Members';
            }
        }
    };
    $scope.projectSettings = {
        enableSearch: true,
        scrollable: true,
        displayProp: 'label', 
        idProp: 'id', 
        externalIdProp: '', // project_id
        selectionLimit: 1,
        smartButtonMaxItems: 1,
        groupByTextProvider: function(groupValue) {
            if (groupValue === 'O') {
                return 'Owned Project';
            } else {
                return 'Joined Project';
            }
        }
    };
    // $scope.clientSettings = {
    //     enableSearch: true,
    //     scrollable: true,
    //     displayProp: 'label', 
    //     idProp: 'label', 
    //     externalIdProp: '', // client_name
    //     selectionLimit: 1
    // };
    // ====================================================================================== //

    // ================================= Date range picker ================================== //
    $scope.datePicker = {};
    $scope.datePicker.date = {
        startDate: moment().format('YYYY/MM/DD'), 
        endDate: moment().format('YYYY/MM/DD')
    };
    // ====================================================================================== //
    var saveReportData;
    var saveCompanyData;
    var saveTeamData;
    var saveProjectData;
    // var saveClientData;
    $scope.tableData = [];
    $scope.showChartTable = false;
    var sendingData = {
        "get_report": {
            "auth_token": ""
        }
    }

    $scope.loadingInit = function() {
    	// check for current user is available
    	var currentUser = localStorageService.get('currentUser');
    	if (!currentUser) {
    		$state.go('main');
    	} else {
            $scope.pageTitle = "Report";
    		$scope.reportPromise = $http.get('http://httpbin.org/delay/31');
    		sendingData.get_report.auth_token = currentUser.data.auth_token;
			getDataServices.getAllData(apiUrl, 'report', sendingData).then(
		        function (result) {
		        	// console.log(result);
                    saveReportData = result;
                    $scope.companyData = [];
                    $scope.teamData = [];
                    $scope.projectData = [];
                    // $scope.clientData = [];
                    
                    addCompanyToFilter(result.owned_companies, true);
                    addCompanyToFilter(result.joined_companies, false);

                    addOwnedTeammemberToFilter(result.teammembers, result.owned_companies);
                    addJoinedTeammemberToFilter(result.joined_managed_projectmembers);

                    addOwnedProjectToFilter(result.owned_companies);
                    addJoinedProjectToFilter(result.joined_companies);

                    // addOwnedClientToFilter(result.owned_companies);
                    // addJoinedClientToFilter(result.joined_companies);
		        },
		        function (error) {
		            // console.log(error.statusText);
                    swal("Oops!", "Please check your internet connection.", "warning");
                }
            ).finally(function() {
                $scope.reportPromise = null;
                saveCompanyData = $scope.companyData;
                saveTeamData = $scope.teamData;
                saveProjectData = $scope.projectData;
                // saveClientData = $scope.clientData;
            });     
        }
    }

    $scope.changeCompany = {
        onItemSelect: function() {
            $scope.reportData.teamModel     = {};
            $scope.reportData.projectModel  = {};
            $('#filter-region .group-filter .company-child button').addClass('color-button');
            $('#filter-region .group-filter .team-child button').removeClass('color-button');
            $('#filter-region .group-filter .project-child button').removeClass('color-button');
        },
        onDeselectAll: function() {
            $('#filter-region .group-filter .company-child button').removeClass('color-button');
        }
    }

    $scope.changeTeam = {
        onItemSelect: function() {
            $scope.reportData.companyModel  = {};
            $scope.reportData.projectModel  = {};
            $('#filter-region .group-filter .company-child button').removeClass('color-button');
            $('#filter-region .group-filter .team-child button').addClass('color-button');
            $('#filter-region .group-filter .project-child button').removeClass('color-button');
        },
        onDeselectAll: function() {
            $('#filter-region .group-filter .team-child button').removeClass('color-button');
        }
    }

    $scope.changeProject = {
        onItemSelect: function() {
            $scope.reportData.companyModel  = {};
            $scope.reportData.teamModel     = {};
            $('#filter-region .group-filter .company-child button').removeClass('color-button');
            $('#filter-region .group-filter .team-child button').removeClass('color-button');
            $('#filter-region .group-filter .project-child button').addClass('color-button');
        },
        onDeselectAll: function() {
            $('#filter-region .group-filter .project-child button').removeClass('color-button');
        }
    }

    $scope.applyFilter = function() {
        var selectedCompany = $scope.reportData.companyModel;
        var selectedTeam = $scope.reportData.teamModel;
        var selectedProject = $scope.reportData.projectModel;
        // var selectedClient = $scope.reportData.clientModel;

        // select only company -> get tasks
        if (!$.isEmptyObject(selectedCompany)) {
            $scope.showChartTable = true;
            if (selectedCompany.own === 'O') {
                getTaskOfCompany(selectedCompany.id, saveReportData.owned_tasks_of_member, saveReportData.owned_companies);
            } else {
                getTaskOfCompany(selectedCompany.id, saveReportData.joined_managed_tasks_of_member, saveReportData.joined_companies);
            }
        }

        // select only team member -> get tasks
        if (!$.isEmptyObject(selectedTeam)) {
            $scope.showChartTable = true;
            if (selectedTeam.own === 'O') {
                getTaskOfTeam(selectedTeam.label, saveReportData.owned_tasks_of_member);
            } else {
                getTaskOfTeam(selectedTeam.label, saveReportData.joined_managed_tasks_of_member);
            }
        }

        // select only project -> get tasks
        if (!$.isEmptyObject(selectedProject)) {
            $scope.showChartTable = true;
            if (selectedProject.own === 'O') {
                getTaskOfProject(selectedProject.id, saveReportData.owned_tasks_of_member);
            } else {
                getTaskOfProject(selectedProject.id, saveReportData.joined_managed_tasks_of_member);
            }
        }

        // select only client -> get tasks
        // if (!$.isEmptyObject(selectedClient)) {
        //     getTaskOfClient(selectedClient.label, selectedClient.company_id);
        // }
    }

    var getTaskOfCompany = function(company_id, tasks_of_member, company_data) {
        var filterStartDate = $scope.datePicker.date.startDate;
        var filterEndDate = $scope.datePicker.date.EndDate;
        var tmpProject = [];
        for (var i = 0; i < saveProjectData.length; i++) {
            if (company_id === saveProjectData[i].company_id) {
                tmpProject.push(saveProjectData[i]);
            }
        };

        var tmpTask = [];
        for (var i = 0; i < tmpProject.length; i++) {
            for (var j = 0; j < tasks_of_member.length; j++) {
                if (tmpProject[i].id === tasks_of_member[j].project_id) {
                    tmpTask.push(tasks_of_member[j]);
                }
            };
        };
        
        var duration = 0;
        var startDate;
        var endDate;
        var startMonth;
        var endMonth;
        for (var i = 0; i < taskTimeData.length; i++) {
            taskTimeData[i].duration = 0;
        };

        for (var i = 0; i < tmpTask.length; i++) { 
            for (var k = 0; k < tmpTask[i].timeentries.length; k++) {
                startDate = tmpTask[i].timeentries[k].start;
                endDate = tmpTask[i].timeentries[k].end;
                startMonth = shortMonth[moment(startDate).get("month")];
                endMonth = shortMonth[moment(endDate).get("month")];

                if (startMonth === endMonth) {
                    duration = getDuration(moment.utc(startDate), moment.utc(endDate));
                    for (var j = 0; j < taskTimeData.length; j++) {
                        if (taskTimeData[j].month === startMonth) {
                            taskTimeData[j].duration += duration;
                        }
                    };
                }
            };
        };
        
        // display at chart, default unit is hour
        var tmpDuration = [];
        for (var i = 0; i < taskTimeData.length; i++) {
            tmpDuration.push(timeConversion(taskTimeData[i].duration, true));
        };
        // console.log(tmpDuration);
        $scope.taskTimeData[0] = tmpDuration;

        // display project data of selected company
        $scope.tableData = [];
        $scope.itemName = "Projects";
        var totalDuration = 0;
        var projectObject = {
            "id": "",
            "itemName": "",
            "totalHours": ""
        }

        for (var i = 0; i < company_data.length; i++) {
            for (var j = 0; j < company_data[i].projects.length; j++) {
                if (company_data[i].projects[j].company_id === company_id) {
                    projectObject.id = company_data[i].projects[j].id;
                    projectObject.itemName = company_data[i].projects[j].project_name;
                    $scope.tableData.push(projectObject);
                    projectObject = {
                        "id": "",
                        "itemName": "",
                        "totalHours": ""
                    }
                }
            };
        };

        for (var i = 0; i < $scope.tableData.length; i++) {
            totalDuration = 0;
            for (var j = 0; j < tasks_of_member.length; j++) { 
                for (var k = 0; k < tasks_of_member[j].timeentries.length; k++) {
                    startDate = tasks_of_member[j].timeentries[k].start;
                    endDate = tasks_of_member[j].timeentries[k].end;
                    startMonth = shortMonth[moment(startDate).get("month")];
                    endMonth = shortMonth[moment(endDate).get("month")];

                    if ($scope.tableData[i].id === tasks_of_member[j].project_id && startMonth === endMonth) {
                        totalDuration += getDuration(moment.utc(startDate), moment.utc(endDate));
                    }
                };
            };
            $scope.tableData[i].totalHours = durationToTime(totalDuration);
        };

        // remove duplicate objects from tableData 
        $scope.tableData = removeDuplicateObject($scope.tableData);
    }

    var getTaskOfTeam = function(user_email, tasks_of_member) {
        var duration = 0;
        var startDate;
        var endDate;
        var startMonth;
        var endMonth;
        for (var i = 0; i < taskTimeData.length; i++) {
            taskTimeData[i].duration = 0;
        };

        for (var i = 0; i < tasks_of_member.length; i++) { 
            for (var k = 0; k < tasks_of_member[i].timeentries.length; k++) {
                startDate = tasks_of_member[i].timeentries[k].start;
                endDate = tasks_of_member[i].timeentries[k].end;
                startMonth = shortMonth[moment(startDate).get("month")];
                endMonth = shortMonth[moment(endDate).get("month")];

                if (user_email === tasks_of_member[i].email && startMonth === endMonth) {
                    duration = getDuration(moment.utc(startDate), moment.utc(endDate));
                    for (var j = 0; j < taskTimeData.length; j++) {
                        if (taskTimeData[j].month === startMonth) {
                            taskTimeData[j].duration += duration;
                        }
                    };
                }
            };
        };
        
        // display at chart, default unit is hour
        var tmpDuration = [];
        for (var i = 0; i < taskTimeData.length; i++) {
            tmpDuration.push(timeConversion(taskTimeData[i].duration, true));
        };
        // console.log(tmpDuration);
        $scope.taskTimeData[0] = tmpDuration;

        // display task data of selected member
        $scope.tableData = [];
        $scope.itemName = "Tasks";
        var totalDuration = 0;
        var taskObject = {
            "id": "",
            "itemName": "",
            "totalHours": ""
        }

        for (var i = 0; i < tasks_of_member.length; i++) {
            if (tasks_of_member[i].email === user_email) {
                taskObject.id = tasks_of_member[i].id;
                taskObject.itemName = tasks_of_member[i].task_name;
                $scope.tableData.push(taskObject);
                taskObject = {
                    "id": "",
                    "itemName": "",
                    "totalHours": ""
                }
            }
        };

        for (var i = 0; i < $scope.tableData.length; i++) {
            totalDuration = 0;
            for (var j = 0; j < tasks_of_member.length; j++) { 
                for (var k = 0; k < tasks_of_member[j].timeentries.length; k++) {
                    startDate = tasks_of_member[j].timeentries[k].start;
                    endDate = tasks_of_member[j].timeentries[k].end;
                    startMonth = shortMonth[moment(startDate).get("month")];
                    endMonth = shortMonth[moment(endDate).get("month")];

                    if ($scope.tableData[i].id === tasks_of_member[j].id && startMonth === endMonth) {
                        totalDuration += getDuration(moment.utc(startDate), moment.utc(endDate));
                    }
                };
            };
            $scope.tableData[i].totalHours = durationToTime(totalDuration);
        };

        // remove duplicate objects from tableData 
        $scope.tableData = removeDuplicateObject($scope.tableData);
    }

    var getTaskOfProject = function(project_id, tasks_of_member) {
        var duration = 0;
        var startDate;
        var endDate;
        var startMonth;
        var endMonth;
        for (var i = 0; i < taskTimeData.length; i++) {
            taskTimeData[i].duration = 0;
        };

        for (var i = 0; i < tasks_of_member.length; i++) { 
            for (var k = 0; k < tasks_of_member[i].timeentries.length; k++) {
                startDate = tasks_of_member[i].timeentries[k].start;
                endDate = tasks_of_member[i].timeentries[k].end;
                startMonth = shortMonth[moment(startDate).get("month")];
                endMonth = shortMonth[moment(endDate).get("month")];

                if (project_id === tasks_of_member[i].project_id && startMonth === endMonth) {
                    duration = getDuration(moment.utc(startDate), moment.utc(endDate));
                    for (var j = 0; j < taskTimeData.length; j++) {
                        if (taskTimeData[j].month === startMonth) {
                            taskTimeData[j].duration += duration;
                        }
                    };
                }
            };
        };
        
        // display at chart, default unit is hour
        var tmpDuration = [];
        for (var i = 0; i < taskTimeData.length; i++) {
            tmpDuration.push(timeConversion(taskTimeData[i].duration, true));
        };
        // console.log(tmpDuration);
        $scope.taskTimeData[0] = tmpDuration;

        // display task data of selected member
        $scope.tableData = [];
        $scope.itemName = "Members";
        var totalDuration = 0;
        var memberObject = {
            "id": "",
            "itemName": "",
            "totalHours": ""
        }

        for (var i = 0; i < tasks_of_member.length; i++) {
            if (tasks_of_member[i].project_id === project_id) {
                memberObject.itemName = tasks_of_member[i].email;
                $scope.tableData.push(memberObject);
                memberObject = {
                    "id": "",
                    "itemName": "",
                    "totalHours": ""
                }
            }
        };

        for (var i = 0; i < $scope.tableData.length; i++) {
            totalDuration = 0;
            for (var j = 0; j < tasks_of_member.length; j++) { 
                for (var k = 0; k < tasks_of_member[j].timeentries.length; k++) {
                    startDate = tasks_of_member[j].timeentries[k].start;
                    endDate = tasks_of_member[j].timeentries[k].end;
                    startMonth = shortMonth[moment(startDate).get("month")];
                    endMonth = shortMonth[moment(endDate).get("month")];

                    if ($scope.tableData[i].itemName === tasks_of_member[j].email && project_id === tasks_of_member[j].project_id && startMonth === endMonth) {
                        totalDuration += getDuration(moment.utc(startDate), moment.utc(endDate));
                    }
                };
            };
            $scope.tableData[i].totalHours = durationToTime(totalDuration);
        };

        // remove duplicate objects from tableData 
        $scope.tableData = removeDuplicateObject($scope.tableData);
    }

    var durationToTime = function(duration) {
        var result;
        if (duration < 60000) {
            result = moment.duration(duration, "ms").format("HH:mm:ss") + "s";
        } else if ((60000 <= duration) && (duration < 3600000)) {
            result = "00:" + moment.duration(duration, "ms").format("HH:mm:ss");
        } else {
            result = moment.duration(duration, "ms").format("HH:mm:ss");
        }
        return result;
    }

    var timeConversion = function(millisec, isHour) {
        var minutes = (millisec / (1000 * 60)).toFixed(2);
        var hours = (millisec / (1000 * 60 * 60)).toFixed(2);
        
        if (isHour) {
            return parseFloat(hours);
        } else {
            return parseFloat(minutes);
        }
    }

    var getDuration = function(startTime, endTime) {
        return endTime.diff(startTime);
    }

    $scope.changeTaskTimeUnit = function() {
        var tmpDuration = [];
        $scope.taskTimeData = [];   
        if ($scope.taskTime.unit === 'minute') {
            for (var i = 0; i < taskTimeData.length; i++) {
                tmpDuration.push(timeConversion(taskTimeData[i].duration, false));
            };
        } else {
            for (var i = 0; i < taskTimeData.length; i++) {
                tmpDuration.push(timeConversion(taskTimeData[i].duration, true));
            };
        }
        $scope.taskTimeData.push(tmpDuration);       
    }

    var addCompanyToFilter = function(company, owned) {
        // true is owned company, false is joined company
        for (var i = 0; i < company.length; i++) {
            var tmpCompany = {
                "id": "",
                "label": "",
                "own": ""
            };
            tmpCompany.id = company[i].id;
            tmpCompany.label = company[i].company_name;
            if (owned) {
                tmpCompany.own = "O";
            } else {
                tmpCompany.own = "J";
            }
            $scope.companyData.push(tmpCompany);
        };
    }

    var addOwnedTeammemberToFilter = function(owned_teammember, owned_company) {
        for (var i = 0; i < owned_teammember.length; i++) {
            var tmpTeammember = {
                "id": "",
                "label": "",
                "company_id": "",
                "own": "O"
            };
            tmpTeammember.id = owned_teammember[i].user_id;
            tmpTeammember.label = owned_teammember[i].email;
            tmpTeammember.company_id = owned_teammember[i].company_id;
            $scope.teamData.push(tmpTeammember);
        };

        var tmpTeammember = {
            "id": "",
            "label": "",
            "company_id": "",
            "own": "O"
        };
        tmpTeammember.id = owned_company[0].user_id;
        tmpTeammember.label = owned_company[0].email;
        tmpTeammember.company_id = owned_company[0].id;
        $scope.teamData.push(tmpTeammember);
    }

    var addJoinedTeammemberToFilter = function(joined_teammember) {
        for (var i = 0; i < joined_teammember.length; i++) {
            var tmpTeammember = {
                "id": "",
                "label": "",
                "company_id": "",
                "own": "J"
            };
            tmpTeammember.id = joined_teammember[i].id;
            tmpTeammember.label = joined_teammember[i].email;
            tmpTeammember.company_id = joined_teammember[i].company_id;
            $scope.teamData.push(tmpTeammember);
        };
    }

    var addOwnedProjectToFilter = function(owned_company) {
        for (var i = 0; i < owned_company.length; i++) {
            for (var j = 0; j < owned_company[i].projects.length; j++) {
                var tmpProject = {
                    "id": "",
                    "label": "",
                    "user_id": "",
                    "company_id": "",
                    "own": ""
                };

                tmpProject.id = owned_company[i].projects[j].id;
                tmpProject.label = owned_company[i].projects[j].project_name;
                tmpProject.user_id = owned_company[i].user_id;
                tmpProject.company_id = owned_company[i].id;
                tmpProject.own = "O";
                $scope.projectData.push(tmpProject);
            };
        };
    }

    var addJoinedProjectToFilter = function(joined_company) {
        for (var i = 0; i < joined_company.length; i++) {
            for (var j = 0; j < joined_company[i].projects.length; j++) {
                var tmpProject = {
                    "id": "",
                    "label": "",
                    "user_id": "",
                    "company_id": "",
                    "own": ""
                };

                tmpProject.id = joined_company[i].projects[j].id;
                tmpProject.label = joined_company[i].projects[j].project_name;
                tmpProject.user_id = joined_company[i].user_id;
                tmpProject.company_id = joined_company[i].id;
                tmpProject.own = "J";
                $scope.projectData.push(tmpProject);
            };
        };
    }    

    var removeDuplicate = function(inputObj) {
        // var result = [];
        // $.each(inputArray, function(i, dup){
        //     if ($.inArray(dup, result) === -1) { 
        //         result.push(dup);
        //     }
        // });
        // return result;
        var tmpObjClient = [];
        var result = [];
        for (var i = 0; i < inputObj.length; i++) {
            tmpObjClient[inputObj[i].label] = inputObj[i].company_id;
        };
        for (var key in tmpObjClient) {
            var tmpClient = {
                "label": "",
                "company_id": ""
            }

            tmpClient.label = key;
            tmpClient.company_id = tmpObjClient[key];
            result.push(tmpClient);
        }
        return result;
    }

    // remove duplicate objects from array
    var removeDuplicateObject = function (object) {
        var result = [];
        object.forEach(function(itm) {
            var unique = true;
            result.forEach(function(itm2) {
                if (_.isEqual(itm, itm2)) unique = false;
            });
            if (unique) result.push(itm);
        });
        return result;
    }

    // var addOwnedClientToFilter = function(owned_company) {
    //     var tmpClientData = [];
    //     for (var i = 0; i < owned_company.length; i++) {
    //         for (var j = 0; j < owned_company[i].projects.length; j++) {
    //             var tmpClient = {
    //                 "label": "",
    //                 "company_id": ""
    //             };

    //             tmpClient.label = owned_company[i].projects[j].client_name;
    //             tmpClient.company_id = owned_company[i].id;
    //             tmpClientData.push(tmpClient);
    //         };
    //     };

    //     $scope.clientData = $scope.clientData.concat(removeDuplicate(tmpClientData));
    // }

    // var addJoinedClientToFilter = function(joined_company) {
    //     var tmpClientData = [];
    //     for (var i = 0; i < joined_company.length; i++) {
    //         for (var j = 0; j < joined_company[i].projects.length; j++) {
    //             var tmpClient = {
    //                 "label": "",
    //                 "company_id": ""
    //             };

    //             tmpClient.label = joined_company[i].projects[j].client_name;
    //             tmpClient.company_id = joined_company[i].id;
    //             tmpClientData.push(tmpClient);
    //         };
    //     };

    //     $scope.clientData = $scope.clientData.concat(removeDuplicate(tmpClientData));
    // }
}]);
