'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

app.controller('MainCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
  var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
  // var apiUrl = 'http://192.168.1.104:3000/api/'; // Tri
  // var apiUrl = 'http://192.168.1.126:3000/api/'; // Thuan

  $scope.loadingInit = function() {
    $scope.formContactResponse = ""; 
    $scope.pageTitle = "Home page";
  }

  $scope.submitContactForm = function() {
    $scope.loading = true;

    var contactData = {
      "contact": {
        "subject": "",
        "email": "",
        "message": ""
      }
    };

    contactData.contact.subject = $scope.formContactData.contactSubject;
    contactData.contact.email = $scope.formContactData.contactEmail;
    contactData.contact.message = $scope.formContactData.contactMessage;
    
    $http({
      method: 'POST',
      url: apiUrl + 'contacts',
      data: $.param(contactData), // pass in data as strings
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
      } // set the headers so angular passing info as form data (not request payload)
    })
    .then(function successCallback(response) {
      // console.log('success');
      // console.log(response);
      $scope.loading = false;
    }, function errorCallback(response) {
      swal("Oops!", "Please check your internet connection.", "warning");
      // console.log('fail');
      // console.log(response);
      $scope.loading = false;
    })
    .finally(function() {
      $scope.formContactResponse = "Thanks for your response";
      $scope.formContactData.contactSubject = null;
      $scope.formContactData.contactEmail = null;
      $scope.formContactData.contactMessage = null;
    });
  };

  $scope.submitSubscribeForm = function() {
    // check to make sure the form is completely valid
    alert("Thank you, your subscription form is submitted");
  };

  // show subscribe
  $scope.showSubscribe = function() {
    $(".subscribe-section").slideToggle(500);
    $("#secret-subscribe").toggleClass('rotate-180');
    $(window).scrollTop($(document).height());
  };

  $('#signin-signup-btn').click(function(event){
        event.stopPropagation();
        event.preventDefault();
        $('#sign').modal('show');
  });

  // Start Watch - Stop Watch Introducing
  var $stopwatch = {
      container: document.getElementById('datetime-picker')
  };
  var counter = 0;
  // display counter time at timer
  $scope.displayTime = function() {
    $stopwatch.container.value = moment().hour(0).minute(0).second(counter++).format('HH:mm:ss');
  }
  // function to start the counter
  $scope.startWatch = function() {
    $('#stop').show();
    $('#start').hide();
    $scope.runClock = setInterval($scope.displayTime, 1000);
  }

  // function to stop the counter
  $scope.stopWatch = function () {
    $('#start').show();
    $('#stop').hide();
    clearInterval($scope.runClock);
    $scope.counter = 0; 
  }
}]);

$(document).ready(function($) {
  // browser window scroll (in pixels) after which the "back to top" link is shown
  var offset = 300,
  //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
  offset_opacity = 1200,
  //duration of the top scrolling animation (in ms)
  scroll_top_duration = 700,
  //grab the "back to top" link
  $back_to_top = $('.cd-top');

  //hide or show the "back to top" link
  $(window).scroll(function() {
    ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible'): $back_to_top.removeClass('cd-is-visible cd-fade-out');
    if ($(this).scrollTop() > offset_opacity) {
      $back_to_top.addClass('cd-fade-out');
    }
  });

  //smooth scroll to top
  $back_to_top.on('click', function(event) {
    event.preventDefault();
    $('body,html').animate({
      scrollTop: 0,
    }, scroll_top_duration);
  });
});