
'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:AccountCtrl
 * @description
 * # AccountCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

// Config for POST data cross domain
app.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
}]);

// redirect to home page when user not logged in
app.run(function($rootScope, $location, localStorageService, $state) {
    var currentUser = localStorageService.get('currentUser');
    if (currentUser) {
        if (currentUser.data.role === 1) {
            $rootScope.isAdmin = true;    
        } else {
            $rootScope.isAdmin = false;
        }
        $rootScope.isLoggedIn = true;
        $rootScope.loggedInEmail = currentUser.data.email;
    } else {
        $state.go('main');
    };
});

app.controller('AccountCtrl', [
    '$scope', 'localStorageService', '$rootScope', '$location', '$facebook', '$auth', '$http', '$state', 
	function ($scope, localStorageService, $rootScope, $location, $facebook, $auth, $http, $state) {
        
    var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
    // var apiUrl = 'http://192.168.1.104:3000/api/';   // Tri
    // var apiUrl = 'http://192.168.1.126:3000/api/';      // Thuan

    // ---------------- BEGIN Facebook intergration ---------------- //
    $scope.loadingInit = function() {
        if($rootScope.isLoggedIn){
            $('#time-traveler-btn-support').show();
            $('#time-traveler-btn-homepage').hide();
        }
    };

    $scope.isLoggedInFB = false;
    $scope.login = function() {
        $facebook.login().then(function() {
            refresh();
        });
    };
    function refresh() {
        $facebook.api("/me").then(
            function(response) {
                // console.log(response.email);
                var fbEmail = response.email;
                var formData = {
                    "user": {
                        "email": "",
                        "password": "",
                        "password_confirmation": ""
                    }
                };
                formData.user.email = response.email;
                formData.user.password = randomPassword();
                formData.user.password_confirmation = formData.user.password;

                $http({
                    method: 'POST',
                    url: apiUrl + 'socialaccount',
                    data: $.param(formData), // pass in data as strings
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    } // set the headers so angular passing info as form data (not request payload)
                })
                .then(function successCallback(response) {
                    // console.log("hello success");
                    // console.log(response);

                    if (response.data.role === 1) {
                        $rootScope.isAdmin = true;    
                    } else {
                        $rootScope.isAdmin = false;
                    }

                    localStorageService.set('currentUser', response);

                    $rootScope.isLoggedIn = true;
                    $rootScope.loggedInEmail = fbEmail;
                    $('#sign').modal('hide');
                    $state.go('timer');

                }, function errorCallback(response) {
                    swal("Oops!", "Please check your internet connection.", "warning");
                    // console.log("fail");
                    // console.log(response);
                });
            },
            function(err) {
                $scope.welcomeMsg = "Please log in";
            });
    };
    refresh();

    var randomPassword = function() {
        var seed = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',0,1,2,3,4,5,6,7,8,9,'{','}','[',']','(',')','/','\\','`','~',',',';',':','.','<','>','!','@','#','$','%','^','&','*'];
        var passwordLength = Math.floor(Math.random() * 10) + 61; // min length = 21, max length = 31
        var password = [passwordLength];

        for (var i = 0; i < passwordLength; i++) {
            var tmp = Math.floor(Math.random() * (seed.length - 1));
            password.push(seed[tmp]);
        };

        return password.join('');
    }
    // ---------------- END Facebook intergration ---------------- //

    // ---------------- BEGIN Google intergration ---------------- //
    $scope.authenticate = function(provider) {
        $auth.authenticate(provider);
        // console.log($auth.email);
    };
    // ---------------- END Google intergration ---------------- //

    // ---------------- BEGIN sending signup/login form ---------------- //
    var formData;
    $scope.loading = false;
    $scope.submitSignupForm = function() {
        $scope.loading = true;
        formData = {
            "user": ""
        };
        formData.user = $scope.formSignupData;

        $http({
            method: 'POST',
            url: apiUrl + 'users',
            data: $.param(formData), // pass in data as strings
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            } // set the headers so angular passing info as form data (not request payload)
        })
        .then(function successCallback(response) {
            $scope.loading = false;
            if (response.data.errors) {
                // if not successful, bind errors to $scope.formSignupResponse variables
                $scope.formSignupResponse = response.data.errors;
            } else {
                // if successful, bind success message to message
                $scope.formSignupResponse = "Congratulation! Your account has been created.";
                $scope.formSignupData.email = null;
                $scope.formSignupData.password = null;
                $scope.formSignupData.password_confirmation = null;
            }
        }, function errorCallback(response) {
            swal("Oops!", "Please check your internet connection.", "warning");
            $scope.loading = false;
            // $scope.formSignupResponse = "Sending data problem. Please try again later.";
        });
    };

    $scope.submitLoginForm = function() {
        $scope.loading = true;
        formData = {
            "session": ""
        };
        formData.session = $scope.formLoginData;

        $http({
            method: 'POST',
            url: apiUrl + 'sessions/login',
            data: $.param(formData), // pass in data as strings
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
        })
        .then(function successCallback(response) {
            // console.log(response);
            $scope.loading = false;
            if (response.data.errors) {
                // if not successful, bind errors to $scope.formLoginResponse variables
                $scope.formLoginResponse = response.data.errors;
            } else {
                if (response.data.role === 1) {
                    $rootScope.isAdmin = true;    
                } else {
                    $rootScope.isAdmin = false;
                }

                localStorageService.set('currentUser', response);

                $rootScope.isLoggedIn = true;
                $rootScope.loggedInEmail = $scope.formLoginData.email;
                $scope.formLoginData.email = null;
                $scope.formLoginData.password = null;
                $('#sign').modal('hide');
                $('#time-traveler-btn-support').show();
                $('#time-traveler-btn-homepage').hide();
                $state.go('timer');
            }
        }, function errorCallback(response) {
            swal("Oops!", "Please check your internet connection.", "warning");
            $scope.loading = false;
            // if not successful, bind errors to $scope.formLoginResponse variables
            // if (response.data) {
            //     $scope.formLoginResponse = response.data.error;
            // }
            // $scope.formLoginResponse = "Sending data problem. Please try again later.";
        });
    };

    $scope.resetPassword = false;
    $scope.showResetPasswordForm = function() {
        $scope.resetPassword = !$scope.resetPassword;
        $("#loginForm").toggle('slow');
        // $("#resetPasswordForm").toggle('slow');

    }

    $scope.submitResetPasswordForm = function() {
        $scope.loading = true;
        var formData = {
            "email": ""
        }
        formData.email = $scope.resetPasswordFormData.email;
        
        $http ({
            method  : 'POST',
            url     : apiUrl + 'users/forgotpassword',
            data    : $.param(formData), // pass in data as strings
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
        })
        .then(function successCallback(response) {
            $scope.loading = false;
            // console.log('success');
            // console.log(response);
            $scope.formLoginResponse = "Please check your email.";
        }, function errorCallback(response) {
            swal("Oops!", "Please check your internet connection.", "warning");
            $scope.loading = false;
            // console.log('fail');
            // console.log(response);
        })
        .finally(function() {
            $scope.resetPasswordFormData = {};
            $scope.resetPasswordFormData.email = null;
        });
    };

    $scope.logoutUser = function() {
        $scope.logoutPromise = $http.get('http://httpbin.org/delay/31');
        $facebook.logout();
        // delete local data of current user
        var tmp = localStorageService.get('currentUser');
        var logoutToken = {
            "auth_token": ""
        }
        logoutToken.auth_token = tmp.data.auth_token;

        $http({
            method: 'DELETE',
            url: apiUrl + 'sessions/logout',
            data: $.param(logoutToken), // pass in data as strings
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            } // set the headers so angular passing info as form data (not request payload)
        })
        .then(function successCallback(response) {

        }, function errorCallback(response) {
            swal("Oops!", "Please check your internet connection.", "warning");

        })
        .finally(function() {
            localStorageService.remove('currentUser');
            $rootScope.isLoggedIn = false;
            $rootScope.isAdmin = false;
            $state.go('main');
            $scope.logoutPromise = null;
        });
    };

    $(document).on('click', function() { 
        $('#sign').modal('hide');
    });
    $("#sign #loginBtn").on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
        $('.nav-tabs a[href="#login"]').tab('show');
    });
    $("#sign #signupBtn").on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
        $('.nav-tabs a[href="#signup"]').tab('show');
    });
    $(".navbar-right #loginBtn").on('click', function(event){ 
        event.stopPropagation();
        event.preventDefault();
        $('#sign').modal('show');
        $('.nav-tabs a[href="#login"]').tab('show');
    });
    $(".navbar-right #signupBtn").on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
        $('#sign').modal('show');
        $('.nav-tabs a[href="#signup"]').tab('show');
    });
    $('#sign').on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
    });
    // Set state for time-traveler-btn
    $('#support-btn').on('click',function(){
        $('#time-traveler-btn-support').show();
        $('#time-traveler-btn-homepage').hide();
    });
    $('#time-traveler-btn-support').on('click',function(){
        if($rootScope.isLoggedIn == false){
            $('#time-traveler-btn-support').hide();
            $('#time-traveler-btn-homepage').show();
        }
    });
    // ---------------- END sending signup/login form ---------------- //
}]);

// Check 2 input passwords is equal
app.directive('pwCheck', function() {
    return {
        require: 'ngModel',
        link: function(scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.pwCheck;
            elem.add(firstPassword).on('keyup', function() {
                scope.$apply(function() {
                    ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
                });
            });
        }
    }
});

// ---------------- BEGIN Facebook config for intergration ---------------- //
app.config(function($facebookProvider) {
    $facebookProvider.setAppId('143256855858019');
});

app.run(function($rootScope) {
    // Load the facebook SDK asynchronously
    (function() {
        // If we've already installed the SDK, we're done
        if (document.getElementById('facebook-jssdk')) {
            return;
        }

        // Get the first script element, which we'll use to find the parent node
        var firstScriptElement = document.getElementsByTagName('script')[0];

        // Create a new script element and set its id
        var facebookJS = document.createElement('script');
        facebookJS.id = 'facebook-jssdk';

        // Set the new script's source to the source of the Facebook JS SDK
        facebookJS.src = '//connect.facebook.net/en_US/all.js';

        // Insert the Facebook JS SDK into the DOM
        firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
    }());
});
// ---------------- END Facebook config for intergration ---------------- //

// ---------------- BEGIN Google config for intergration ---------------- //
app.config(function($authProvider) {
    // $authProvider.facebook({
    //   clientId: 'Facebook App ID'
    // });

    $authProvider.google({
        clientId: '622696710458-981vbsk97dbbgm1jt6crorsn81k4gnba.apps.googleusercontent.com',
        scope: ['email']
    });
});
// ---------------- END Google config for intergration ---------------- //