'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:CompanyCtrl
 * @description
 * # CompanyCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

// Config for POST data cross domain
app.config(function($routeProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});

app.controller('CompanyCtrl', [
	'$scope', '$location', '$http', 'localStorageService', 'getDataServices', '$state',
	function ($scope, $location, $http, localStorageService, getDataServices, $state) {
		
	var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
    // var apiUrl = 'http://192.168.1.104:3000/api/';	// Tri
    // var apiUrl = 'http://192.168.1.126:3000/api/';		// Thuan
 	
    // get data from server
    var retrievedData;
    var sendingData = {
    	"get_companies": {
    		"auth_token":""
    	}
    };

    $scope.loadingInit = function() {
    	// check for current user is available    	
    	var currentUser = localStorageService.get('currentUser');
    	if (!currentUser) {
    		$state.go('main');
    	} else {
    		$scope.pageTitle = "Company Management"
    		$scope.companyPromise = $http.get('http://httpbin.org/delay/31');
    		sendingData.get_companies.auth_token = currentUser.data.auth_token;
    		// sendingData.get_companies.auth_token = "RsHLy16UsXa_vXeXxLV4";
			getDataServices.getAllData(apiUrl, 'managecompanies', sendingData).then(
		        function (result) {
		            addCompanyToTable(result.companies, result.departments);
		        },
		        function (error) {
		            // console.log(error.statusText);
		            swal("Oops!", "Please check your internet connection.", "warning");
		        }
		    ).finally(function() {
		    	$scope.companyPromise = null;
		    });	    
		}
    }

	// checkbox for select all in timer page
	var checked = false;
	$scope.selectAll = function() {
	  	angular.forEach($scope.tableData, function (tdata) {
	    	tdata.selectedCell = !checked;
	  	});
	  	checked = !checked;
	}

	/*Project Table*/	
	$scope.tableData = [];

	$scope.addNewCompany = function() {
		var formData = $scope.formNewCompanyData;
		var currentUser = localStorageService.get('currentUser');
		var newCompany = {
			"create_company": {
				"auth_token": "",
				"company_name": "",
				"department_names": []
			}
		} 

		var newDepartment = [];
		if (formData.department) {
			for (var i = 0; i < formData.department.length; i++) {
				newDepartment.push(formData.department[i].text);
			};
		}
		newCompany.create_company.auth_token = currentUser.data.auth_token;
		newCompany.create_company.company_name = formData.company_name;
		newCompany.create_company.department_names = newDepartment;
		
		var tmpCompany = newCompany.create_company.company_name.trim().toUpperCase();
		var tmpExist = false;
		for (var i = 0; i < $scope.tableData.length; i++) {
			if (tmpCompany === $scope.tableData[i].companyName.trim().toUpperCase()) {
				tmpExist = true;
				swal("Oops!", "The company name is already existed", "warning");
				break;
			}
		};

		if (!tmpExist) {
			$http({
	            method: 'POST',
	            url: apiUrl + 'companies',
	            data: $.param(newCompany), // pass in data as strings
	            headers: {
	                'Content-Type': 'application/x-www-form-urlencoded'
	            } // set the headers so angular passing info as form data (not request payload)
	        })
	        .then(function successCallback(response) {
	        	
	        }, function errorCallback(response) {
	        	swal("Oops!", "Please check your internet connection.", "warning");
	            // console.log('fail');
	            // console.log(response);
	        })
	        .finally(function() {
	        	$('#new-company').modal('hide');
	        	$scope.tableData = [];
	        	$scope.loadingInit();
	        });
		}
	};

	// function to add company to table data to display
	var companyId = 0;
	var addCompanyToTable = function(companies, departments) {
		var tmpCompany = companies;
		var tmpDepartment = [];

		for (var i = 0; i < companies.length; i++) {
			tmpCompany = companies[i].company_name;
			for (var j = 0; j < departments.length; j++) {
				if (companies[i].id === departments[j].company_id) {
					tmpDepartment.push(departments[j].department_name);
				}
			}
			var companyData = {
				"id": companies[i].id,
				"companyName": tmpCompany,
				"department": tmpDepartment.join(', ')
			};
			$scope.tableData.push(companyData);
			tmpDepartment = [];
		};
	};

	$scope.openModalEditCompany = function(index) { 
		$scope.formEditCompanyData = {};
		for (var i = 0; i < $scope.tableData.length; i++) {
			if ($scope.tableData[i].id === index) {
				$scope.formEditCompanyData.company_id = $scope.tableData[i].id;
				$scope.formEditCompanyData.company_name = $scope.tableData[i].companyName;
				$scope.formEditCompanyData.department = $scope.tableData[i].department.split(", ");
				$('#edit-company').modal('show');
			}
		};
	}

	$scope.openModalNewCompany = function() {
		$scope.formNewCompanyData = {};
		$scope.formNewCompanyData.company_name = null;
		$scope.formNewCompanyData.department = null;
	}

	$scope.editCompany = function() {
		var formData = {  
		    "update_company": {  
		        "company_id": "",
		        "company_name": "",
		        "department_names": []
		    }
		}

		formData.update_company.company_id = $scope.formEditCompanyData.company_id;
		formData.update_company.company_name = $scope.formEditCompanyData.company_name;
		for (var i = 0; i < $scope.formEditCompanyData.department.length; i++) {
			formData.update_company.department_names.push($scope.formEditCompanyData.department[i].text);
		};

		$http({
            method: 'POST',
            url: apiUrl + 'update_company_departments',
            data: $.param(formData), // pass in data as strings
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            } // set the headers so angular passing info as form data (not request payload)
        })
        .then(function successCallback(response) {
        	// console.log('success');
            // console.log(response);
        }, function errorCallback(response) {
        	swal("Oops!", "Please check your internet connection.", "warning");
            // console.log('fail');
            // console.log(response);
        })
        .finally(function() {
        	$scope.tableData = [];
        	$scope.loadingInit();
        });
	}

	$scope.removeCompany = function(index) {
		swal({
            title: "Are you sure?",
            text: "We would not recommend to DELETE a COMPANY. All of your company's data will be ERASED including Projects, Departments and Team members",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#f06722",
            confirmButtonText: "Remove company and DELETE DATA!",
            cancelButtonText: "Sorry, I missclicked",
            closeOnConfirm: false
        },
        function(isConfirm){
            if (isConfirm) {
            	var delId = {
		        	"delete_companies": {
		        		"ids": []
		        	}
		        };
            	swal("Deleted!", "A company has been deleted.", "success");
            	for (var i = 0; i < $scope.tableData.length; i++) {
            		if ($scope.tableData[i].id === index) {
                		delId.delete_companies.ids.push($scope.tableData[i].id);
                		break;
            		}
            	};
                $http({
                  	method: 'POST',
                  	url 	: apiUrl + 'deletecompanies',
	              	data    : $.param(delId),
                  	headers: {
                      	'Content-Type': 'application/x-www-form-urlencoded'
                  	} // set the headers so angular passing info as form data (not request payload)
                })
                .then(function successCallback(response) {

                }, function errorCallback(response) {
                	swal("Oops!", "Please check your internet connection.", "warning");

                })
                .finally(function() {
                  	$scope.tableData = [];
                  	$scope.loadingInit();
                });
            }
        });
	}

	$scope.removeAll = function(){
		var tmpCheck = false;
		angular.forEach($scope.tableData, function (tdata) {
	    	if (tdata.selectedCell) {
	    		tmpCheck = true;
	    	}
	  	});

		if (tmpCheck) {
			swal({
		        title: "Are you sure?",
		        text: "We would not recommend to DELETE these COMPANIES. All of your company's data will be ERASED including Projecs, Departments and Team members",
		        type: "warning",
		        showCancelButton: true,
		        confirmButtonColor: "#f06722",
		        confirmButtonText: "Remove companies and DELETE DATA!",
		        cancelButtonText: "Sorry, I missclicked",
		        closeOnConfirm: false
		    },
		    function(isConfirm) {
		        if (isConfirm) {
			        var delIds = {
			        	"delete_companies": {
			        		"ids": []
			        	}
			        };
		            swal("Deleted!", "Your companies has been deleted.", "success");
		            for (var i = 0; i < $scope.tableData.length; i++) {
		                if ($scope.tableData[i].selectedCell == true) {
		                    delIds.delete_companies.ids.push($scope.tableData[i].id);
		                }
		            }
		            $http({
		              	method	: 'POST',
		              	url 	: apiUrl + 'deletecompanies',
		              	data    : $.param(delIds),
		              	headers : {
		                  	'Content-Type': 'application/x-www-form-urlencoded'
		              	} // set the headers so angular passing info as form data (not request payload)
		            })
		            .then(function successCallback(response) {

		            }, function errorCallback(response) {
		            	swal("Oops!", "Please check your internet connection.", "warning");

		            })
		            .finally(function() {
		              	$scope.tableData = [];
		              	$scope.loadingInit();
		            });
		        }
		    });
		} else {
			swal("Oops!", "Please choose at least ONE item to delete", "warning");
		}
    }
	
	// sort table
	$scope.order = function(predicate) {
        // $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        if ($scope.predicate === predicate) {
        	$scope.reverse = !$scope.reverse;
        	// $scope.tableData.reverse();
        } else {
        	$scope.reverse = false;
        	// $scope.tableData.sort();
        }
        $scope.predicate = predicate;
    };
}]);
