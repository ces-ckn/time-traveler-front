'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:FeedbackCtrl
 * @description
 * # FeedbackCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

// Config for POST data cross domain
app.config(function($routeProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});

app.controller('FeedbackCtrl', [
	'$scope', 'localStorageService', '$state', '$http',
	function ($scope, localStorageService, $state, $http) {

	var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
    // var apiUrl = 'http://192.168.1.104:3000/api/';	// Tri
    // var apiUrl = 'http://192.168.1.126:3000/api/';	// Thuan

	$scope.tableData = [];
	
	$scope.loadingInit = function() {
    	// check for current user is available
    	var currentUser = localStorageService.get('currentUser');
    	if (!currentUser || currentUser.data.role === 0) {
    		$state.go('main');
    	} else {
    		$scope.pageTitle = "Feedback Management";
    		$scope.feedbackPromise = $http.get('http://httpbin.org/delay/31');
			$http({
	            method: 'GET',
	            cache: false,
	            url: apiUrl + 'contacts',
	            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	        })
		    .then(function successCallback(response) { 
		      	for (var i = 0; i < response.data.contacts.length; i++) {
		      		var tmpResponse = response.data.contacts[i];
		      		var tmpFeedback = {
			      		"id": "",
			      		"user_email": "",
			      		"subject": "",
			      		"message": "",
			      		"reply": ""
			      	}

			      	tmpFeedback.id = tmpResponse.id;
			      	tmpFeedback.user_email = tmpResponse.email;
			      	tmpFeedback.subject = tmpResponse.subject;
			      	tmpFeedback.message = tmpResponse.message;
			      	tmpFeedback.reply = (tmpResponse.reply === 1) ? "Replied" : "Not replied";
			      	$scope.tableData.push(tmpFeedback);
		      	};
		    }, function errorCallback(response) {
		      	// console.log('fail');
		      	// console.log(response);
		      	swal("Oops!", "Please check your internet connection.", "warning");
		    })
		    .finally(function() {
		    	$scope.feedbackPromise = null;
		    });
		}
    }
    
    $scope.openModalReplyFeedback = function(index) {
    	$scope.formReplyFeedbackData = {};
    	$scope.formReplyFeedbackData.replyMessage = null;
    	for (var i = 0; i < $scope.tableData.length; i++) {
    		if ($scope.tableData[i].id === index) {
    			$scope.formReplyFeedbackData.user_email = $scope.tableData[i].user_email;
    			$scope.formReplyFeedbackData.id = $scope.tableData[i].id;
    		}
    	};
    }

    $scope.removeFeedback = function(index) {
		swal({
            title: "Are you sure?",
            text: "Your feedback will be deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#f06722",
            confirmButtonText: "Yes, I'm sure.",
            cancelButtonText: "Sorry, I missclicked",
            closeOnConfirm: false
        },
        function(isConfirm){
            if (isConfirm) {
            	$http({
			        method: 'DELETE',
			        url: apiUrl + 'contacts/' + index,
			        headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
			    })
			    .then(function successCallback(response) {

			    }, function errorCallback(response) {
			    	swal("Oops!", "Please check your internet connection.", "warning");
			    })
			    .finally(function() {
	                swal("Deleted!", "A feedback has been deleted.", "success");
	                $scope.tableData = [];
	                $scope.loadingInit();
			    });
            }
        });
	}

	$scope.removeFeedbackAll = function() {
		var currentUser = localStorageService.get('currentUser');
        var tmpCheck = false;
        angular.forEach($scope.tableData, function (tdata) {
            if (tdata.selectedCell) {
                tmpCheck = true;
            }
        });

        if (tmpCheck) {
			swal({
	            title: "Are you sure?",
	            text: "Your feedbacks will be deleted.",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#f06722",
	            confirmButtonText: "Yes, I'm sure.",
	            cancelButtonText: "Sorry, I missclicked",
	            closeOnConfirm: false
	        },
	        function(isConfirm){
	            if (isConfirm) {
					var tmpFeedback = {
						"delete_feedbacks": {
							"ids": []
						}
					}
	            	for (var i = 0; i < $scope.tableData.length; i++) {
					    if ($scope.tableData[i].selectedCell == true) {
					        tmpFeedback.delete_feedbacks.ids.push($scope.tableData[i].id);
					    }
					}
					
	            	$http({
				        method: 'POST',
				        url: apiUrl + 'delete_feedbacks',
				        data: $.param(tmpFeedback),
				        headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
				    })
				    .then(function successCallback(response) {
				    	// console.log('success');
				    	// console.log(response);
				    }, function errorCallback(response) {
				    	swal("Oops!", "Please check your internet connection.", "warning");
				    	// console.log('fail');
				    	// console.log(response);
				    })
				    .finally(function() {
		                swal("Deleted!", "A feedback has been deleted.", "success");
		                $scope.tableData = [];
		                $scope.loadingInit();
				    });
	            }
	        });
		} else {
            swal("Oops!", "Please choose at least ONE item to delete", "warning");
        }
	}

	$scope.replyFeedback = function() {
		$scope.loading = true;
		var replyData = {
			"reply_feedback": {
				"feedback_id": "", 
				"message": ""
			}
		};
		replyData.reply_feedback.feedback_id = $scope.formReplyFeedbackData.id;
		replyData.reply_feedback.message = $scope.formReplyFeedbackData.replyMessage;

		$http({
	        method: 'POST',
	        url: apiUrl + 'reply_feedback',
	        data: $.param(replyData),
	        headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
	    })
	    .then(function successCallback(response) {
	    	// console.log('success');
	    	// console.log(response);
	    }, function errorCallback(response) {
	    	swal("Oops!", "Please check your internet connection.", "warning");
	    	// console.log('fail');
	    	// console.log(response);
	    })
	    .finally(function() {
	    	$('#reply-feedback').modal('toggle');
            $scope.tableData = [];
            $scope.loadingInit();
            $scope.loading = false;
	    });
	}

    // checkbox for select all in timer page
	var checked = false;
	$scope.selectAll = function() {
	  	angular.forEach($scope.tableData, function (tdata) {
	    	tdata.selectedCell = !checked;
	  	});
	  	checked = !checked;
	}

	// sort table
	$scope.order = function(predicate) {
        if ($scope.predicate === predicate) {
        	$scope.reverse = !$scope.reverse;
        } else {
        	$scope.reverse = false;
        	$scope.tableData.sort();
        }
        $scope.predicate = predicate;
    };
}]);
