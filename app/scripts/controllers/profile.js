'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

// Config for POST data cross domain
app.config(function($routeProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});

app.controller('ProfileCtrl', [
    '$scope', 'localStorageService', '$http',
    function ($scope, localStorageService, $http) {

    var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
    // var apiUrl = 'http://192.168.1.104:3000/api/';   // Tri
    // var apiUrl = 'http://192.168.1.126:3000/api/';   // Thuan

    $scope.formProfileResponse = {};

    $scope.loadingInit = function() {
        var currentUser = localStorageService.get('currentUser');
        if (currentUser) {
            $scope.editSuccess = false;
            $scope.formProfile = {};
            $scope.formProfile.email = currentUser.data.email;
        }
    }

    $scope.editProfile = function() {
        $scope.loading = true;
        var currentUser = localStorageService.get('currentUser');
        var profileData = {
            "user": {
                "auth_token": "", 
                "new_password": "", 
                "password_confirmation": ""
            }
        }

        profileData.user.auth_token = currentUser.data.auth_token;
        profileData.user.new_password = $scope.formProfile.newPassword;
        profileData.user.password_confirmation = $scope.formProfile.password_confirmation;
        // console.log(profileData);

        $http({
            method: 'POST',
            url: apiUrl + 'change_password',
            data: $.param(profileData),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
        })
        .then(function successCallback(response) {
            // console.log('success');
            // console.log(response);
            $scope.editMessage = true;
            $scope.formProfileResponse.message = "You've changed your password successfully.";
            $scope.formProfileResponse.color = 'green';
        }, function errorCallback(response) {
            // console.log('fail');
            // console.log(response);
            $scope.editMessage = true;
            $scope.formProfileResponse.message = "Cannot change your password, please try again.";
            $scope.formProfileResponse.color = 'red';
        })
        .finally(function() {
            $scope.loading = false;
            $scope.formProfile.newPassword = null;
            $scope.formProfile.password_confirmation = null;
        });
        
    }
}]);

// Check 2 input passwords is equal
app.directive('pwCheck', function() {
    return {
        require: 'ngModel',
        link: function(scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.pwCheck;
            elem.add(firstPassword).on('keyup', function() {
                scope.$apply(function() {
                    ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
                });
            });
        }
    }
});