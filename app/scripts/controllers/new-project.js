'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:NewProjectCtrl
 * @description
 * # NewProjectCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

app.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

// config for UI select
app.config(function (uiSelectConfig) {
  uiSelectConfig.theme = 'selectize';
  uiSelectConfig.resetSearchInput = true;
  uiSelectConfig.appendToBody = true;
});

// configuration for cross domain
app.config(function ($routeProvider, $httpProvider){
  $httpProvider.defaults.useXDomain = true;
  $httpProvider.defaults.withCredentials = true;
  delete $httpProvider.defaults.headers.common["X-Requested-With"];
  $httpProvider.defaults.headers.common["Accept"] = "application/json";
  $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});

app.controller('NewProjectCtrl', [
  '$scope', '$http', 'localStorageService', 'getDataServices', '$state',
  function ($scope, $http, localStorageService, getDataServices, $state) {

	var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
  // var apiUrl = 'http://192.168.1.104:3000/api/'; // Tri
  // var apiUrl = 'http://192.168.1.126:3000/api/';  // Thuan

  // initialize form data variable
  $scope.formNewProjectData = {};

  $scope.createSuccess = false;
	$scope.loading = false;
  	
  $scope.availableCompanies = [];
	$scope.availableClients = [];
	$scope.availableMembers = [];
  var allClients = [];
  var allMembers = [];

  // get data from server
  var retrievedData;
  var sendingData = {
    "get_values_of_new_project": {
      "auth_token":""
    }
  };
  
  $scope.loadingInit = function() {
    var currentUser = localStorageService.get('currentUser');
    if (!currentUser) {
      $state.go('main');
    } else {
      $scope.pageTitle = "New Project";
      $scope.newProjectPromise = $http.get('http://httpbin.org/delay/31');
      // get all companies and clients, members
      sendingData.get_values_of_new_project.auth_token = currentUser.data.auth_token; 
      getDataServices.getAllData(apiUrl, 'values_of_new_project', sendingData).then(
        function (result) {
          // console.log(result);
          addCompanyToForm(result.owned_companies, true);
          addCompanyToForm(result.joined_companies, false);

          allClients[1] = result.owned_clients_name;
          allClients[0] = result.joined_clients_name;

          allMembers[0] = result.owned_teammembers;
          allMembers[1] = result.joined_teammembers;

          $scope.formNewProjectData.projectManager = currentUser.data.email;
        },
        function (error) {
          // console.log(error.statusText);
          swal("Oops!", "Please check your internet connection.", "warning");
        }
      ).finally(function(){
        $scope.newProjectPromise = null;
      });      

      // set default project color
      $scope.formNewProjectData.project_color = '#4DC3FF';
    }
  }

  var addCompanyToForm = function(company, isOwned) {
    if (company) {
      for (var i = 0; i < company.length; i++) {
        var tmpCompany = {
          "company_id": "",
          "company_name": ""
        }
        tmpCompany.company_id = company[i].id;
        if (isOwned) {
          tmpCompany.company_name = 'Owned - ' + company[i].company_name;
        } else {
          tmpCompany.company_name = 'Joined - ' + company[i].company_name;
        }
        $scope.availableCompanies.push(tmpCompany);
      };
    }
  }

  var addClientToForm = function(client, company_id) {
    if (client.length > 0) { 
      for (var i = 0; i < client.length; i++) {
        if (client[i].company_id === company_id) {
          $scope.availableClients.push(client[i].client_name);
        }
      };
    }
  }

  var addMemberToForm = function(member, company_id) {
    var currentUser = localStorageService.get('currentUser');
    if (member.length > 0) { 
      for (var i = 0; i < member.length; i++) {
        if (member[i].company_id === company_id && currentUser.data.id !== member[i].user_id) {
          var tmpMember = {
            "teammember_id": "",
            "email": ""
          }
          tmpMember.teammember_id = member[i].id;
          tmpMember.email = member[i].email;
          $scope.availableMembers.push(tmpMember);
        }
      };
    }
  }

  $scope.changeCompany = function() {
    var company_id = parseInt($('.new-project-section #select-company').val());
    $scope.availableClients = [];
    $scope.formNewProjectData.client_name = null;
    $scope.availableMembers = [];
    $scope.formNewProjectData.members = null;
    // add clients 
    for (var i = 0; i < allClients.length; i++) {
      addClientToForm(allClients[i], company_id);
    };

    // add members
    for (var i = 0; i < allMembers.length; i++) { 
      addMemberToForm(allMembers[i], company_id);
    }; 
  }

  /*Project  Color Table*/
  $scope.projectColorRow = [
    "#4DC3FF", "#BC85E6", "#DF7BAA", "#F68D38",
    "#B27636", "#8AB734", "#14A88E", "#268BB5",
    "#6668B4", "#A4506C", "#67412C", "#3C6526", 
    "#094558", "#BC2D07", "#999999", "#F4FA58"
  ];
  $scope.setProjectColor = function(colorId){
    $("#project-color-div").css("background-color", $scope.projectColorRow[colorId]);
    $scope.formNewProjectData.project_color = $scope.projectColorRow[colorId];
    // $scope.projectColor = $scope.projectColorRow[colorId] + "; color: white;";
  }

	$scope.submitFormNewProject = function() {
    var projectData = {
      "create_project": {
        "auth_token": "",
        "project": {
          "project_name": "",
          "company_id": "",
          "client_name": "",
          "color": "",
          "desc": ""
        },
        "teammember_ids": []
      }
    };
		$scope.loading = true;
    var currentUser = localStorageService.get('currentUser');

    projectData.create_project.auth_token = currentUser.data.auth_token;
    projectData.create_project.project.project_name = $scope.formNewProjectData.project_name;
    projectData.create_project.project.company_id = parseInt($('.new-project-section #select-company').val());
    projectData.create_project.project.client_name = ($scope.formNewProjectData.client_name) ? $scope.formNewProjectData.client_name[0] : "";
    projectData.create_project.project.color = $scope.formNewProjectData.project_color;
    projectData.create_project.project.desc = $scope.formNewProjectData.desc;
    if ($scope.formNewProjectData.members) {
      for (var i = 0; i < $scope.formNewProjectData.members.length; i++) {
        projectData.create_project.teammember_ids.push($scope.formNewProjectData.members[i].teammember_id);
      };
    }
    // console.log(projectData);
		$http ({
      	method  : 'POST',
      	url     : apiUrl + 'projects',
      	data    : $.param(projectData), // pass in data as strings
      	headers : {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
    })
    .then(function successCallback(response) {
	    $scope.formNewProjectResponse = "Congratulation! Your project has been created.";
	    $scope.formNewProjectData.project_name = null;
	    $scope.formNewProjectData.companies = null;
	    $scope.formNewProjectData.client_name = null;
	    $scope.formNewProjectData.members = null;
	    $scope.formNewProjectData.desc = null;
	    $scope.createSuccess = true;
	    $scope.loading = false;
		}, function errorCallback(response) {
      swal("Oops!", "Please check your internet connection.", "warning");
	    $scope.createSuccess = false;
	    $scope.loading = false;
		});
	};

  $scope.cancelFormNewProject = function() {
    $state.go('project');
  }
}]);

