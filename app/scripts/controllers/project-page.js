'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:ProjectPageCtrl
 * @description
 * # ProjectPageCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

// Config for POST data cross domain
app.config(function($routeProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});

app.controller('ProjectPageCtrl', [
  '$http', '$scope', '$location', '$state', 'localStorageService', 'getDataServices', 'shareProperties', 
  function ($http, $scope, $location, $state, localStorageService, getDataServices, shareProperties) {
	
  var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
  // var apiUrl = 'http://192.168.1.104:3000/api/'; // Tri
  // var apiUrl = 'http://192.168.1.126:3000/api/';  // Thuan

  // get data from server
  var retrievedData;
  var saveTableData;
  var sendingData = {
    "get_companies": {
      "auth_token":""
    }
  };
  $scope.availableCompanies = [];

  $scope.loadingInit = function() {
    var currentUser = localStorageService.get('currentUser');
    
    if (!currentUser) {
      $state.go('main');
    } else {
      $scope.pageTitle = "Project Management";
      // get all projects data
      $scope.projectPagePromise = $http.get('http://httpbin.org/delay/31');
      sendingData.get_companies.auth_token = currentUser.data.auth_token;
      getDataServices.getAllData(apiUrl, 'manage_projects_members', sendingData).then(
        function (result) { console.log(result);
          displayTableData(result.owned_companies, true, result.owned_projectmembers);
          displayTableData(result.joined_companies, false, result.joined_projectmembers);
          saveTableData = $scope.tableData; 
        },
        function (error) {
          // handle errors here
          // console.log(error.statusText); 
          swal("Oops!", "Please check your internet connection.", "warning");
        }
      ).finally(function(){
        $scope.projectPagePromise = null;
        console.log($scope.tableData);
      });
      
      // display edited info into table (if available)
      // var editedProject = shareProperties.getProperty();
    }
  }

  var displayTableData = function(company, isOwned, projectMember) { 
    var currentUser = localStorageService.get('currentUser');
    if (company) {
      var tmpProject = {
        "id":"",
        "projectName": "",
        "projectManager": "",
        "projectManager_id": "",
        "client": "",
        "color": "",
        "team": [],
        "desc": "",
        "company": {
          "id": "",
          "company_name": "",
          "company_admin": "",
          "company_owned": false
        }
      };
      
      for (var i = 0; i < company.length; i++) {
        var tmpCompany = {
          "id": "",
          "company_name": "",
          "numberOfProject": ""
        }

        tmpCompany.id = company[i].id;
        if (isOwned) {
          tmpCompany.company_name = 'Owned - ' + company[i].company_name;
        } else {
          tmpCompany.company_name = 'Joined - ' + company[i].company_name;
        }
        tmpCompany.numberOfProject = company[i].projects.length;
        $scope.availableCompanies.push(tmpCompany);

        for (var j = 0; j < company[i].projects.length; j++) {
          // get project data
          var tmp = company[i].projects[j];
          tmpProject.id = tmp.id;
          tmpProject.projectName = tmp.project_name;
          tmpProject.client = tmp.client_name;
          tmpProject.color = tmp.color;
          tmpProject.desc = tmp.desc;
          tmpProject.company.id = company[i].id;
          tmpProject.company.company_name = company[i].company_name;
          tmpProject.company.company_admin = company[i].email;
          tmpProject.company.company_owned = isOwned;

          var tmpTeam = [];
          for (var k = 0; k < projectMember.length; k++) {
            if (projectMember[k].project_id === company[i].projects[j].id) {
              var teamObj = {
                "teammember_id": "",
                "email": ""
              }
              teamObj.teammember_id = projectMember[k].teammember_id;
              teamObj.email = projectMember[k].email;
              tmpTeam.push(teamObj);
              
              if (projectMember[k].project_manager === 1) {
                tmpProject.projectManager = projectMember[k].email;
                tmpProject.projectManager_id = projectMember[k].teammember_id;
              }
            }
          };

          tmpProject.team = tmpTeam;
          if (tmpProject.id !== "") {
            $scope.tableData.push(tmpProject);
          }

          // reset project object
          tmpProject = {
            "id":"",
            "projectName": "",
            "projectManager": "",
            "projectManager_id": "",
            "client": "",
            "color": "",
            "team": [],
            "desc": "",
            "company": {
              "id": "",
              "company_name": "",
              "company_admin": "",
              "company_owned": false
            }
          };
        };
      };
    }
  }

  // checkbox for select all in timer page
	var checked = false;
	$scope.selectAll = function() {
  	var currentUser = localStorageService.get('currentUser');
    angular.forEach($scope.tableData, function (tdata) {
      if (tdata.company.company_owned || !tdata.company.company_owned) {
        if ((tdata.projectManager === currentUser.data.email) || (tdata.company.company_admin === currentUser.data.email)) {
          tdata.selectedCell = !checked;
        }
      }
    });
    checked = !checked;
	}

  $scope.selectOne = function(index) {
    var currentUser = localStorageService.get('currentUser');
    for (var i = 0; i < $scope.tableData.length; i++) {
      if ($scope.tableData[i].id === index) {
        if ($scope.tableData[i].company.company_owned || !$scope.tableData[i].company.company_owned) {
          if (($scope.tableData[i].projectManager === currentUser.data.email) || ($scope.tableData[i].company.company_admin === currentUser.data.email)) {

          } else { 
            $('#selectedCell-' + index).prop('checked', false);
          }
        } else { 
          $('#selectedCell-' + index).prop('checked', false);
        }
      }
    };
  }

  $scope.tableData = [];
	
  // catch event when you change company
  $scope.changeCompany = function() {
    if ($('.project-section #select-company').val() === "") {
      $scope.tableData = [];
      if (saveTableData) {
        for (var i = 0; i < saveTableData.length; i++) {
          $scope.tableData.push(saveTableData[i]);
        };
      }
    } else {
      var index = parseInt($('.project-section #select-company').val());
      $scope.tableData = [];

      if (saveTableData) {
        for (var i = 0; i < saveTableData.length; i++) { 
          if (saveTableData[i].company.id === index) { 
            $scope.tableData.push(saveTableData[i]);
          }
        };
      }
    }
  }

  // create a new project
  $scope.newProject = function() {
    // $location.path('/new-project');
    $state.go('new-project');
  }

  // edit a project
  $scope.editProject = function(index) {
    var currentUser = localStorageService.get('currentUser');
    for (var i = 0; i < $scope.tableData.length; i++) {
      if ($scope.tableData[i].id === index) {
        if ($scope.tableData[i].company.company_owned || !$scope.tableData[i].company.company_owned) {
          if (($scope.tableData[i].projectManager === currentUser.data.email) || ($scope.tableData[i].company.company_admin === currentUser.data.email)) {
            shareProperties.setProperty($scope.tableData[i]);
            $state.go('edit-project');
            break;
          } else {
            swal("Oops!", "You do not have authorization on this project!", "warning");
          }
        } else {
          swal("Oops!", "You do not have authorization on this project!", "warning");
        }
      }
    };
  }

  $scope.removeProject = function(index) {
    var currentUser = localStorageService.get('currentUser');
    for (var i = 0; i < $scope.tableData.length; i++) {
      if ($scope.tableData[i].id === index) {
        if ($scope.tableData[i].company.company_owned || !$scope.tableData[i].company.company_owned) {
          if (($scope.tableData[i].projectManager === currentUser.data.email) || ($scope.tableData[i].company.company_admin === currentUser.data.email)) {
            var delId = {
              "delete_projects": {
                "ids": []
              }
            };
            delId.delete_projects.ids.push($scope.tableData[i].id);
            swal({
              title: "Are you sure?",
              text: "We would not recommend to DELETE a PROJECT. All of your Tasks will be ERASED",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#f06722",
              confirmButtonText: "Remove project and DELETE DATA!",
              cancelButtonText: "Sorry, I missclicked",
              closeOnConfirm: false
            },
            function(isConfirm) {
              if (isConfirm) {
                swal("Deleted!", "A project has been deleted.", "success");
                $http({
                  method: 'POST',
                  url     : apiUrl + 'delete_projects',
                  data    : $.param(delId),
                  headers: {
                      'Content-Type': 'application/x-www-form-urlencoded'
                  } // set the headers so angular passing info as form data (not request payload)
                })
                .then(function successCallback(response) {

                }, function errorCallback(response) {
                  swal("Oops!", "Please check your internet connection.", "warning");
                })
                .finally(function() {
                  $scope.tableData = [];
                  $scope.availableCompanies = [];
                  $scope.loadingInit();
                });
              }
            });
            break;
          } else {
            swal("Oops!", "You do not have authorization on this project!", "warning");
          }
        } else {
          swal("Oops!", "You do not have authorization on this project!", "warning");
        }
      }
    };
  }

  $scope.removeAll = function(){
    var tmpCheck = false;
    angular.forEach($scope.tableData, function (tdata) {
      if (tdata.selectedCell) {
        tmpCheck = true;
      }
    });

    if (tmpCheck) {
      swal({
        title: "Are you sure?",
        text: "We would not recommend to DELETE these PROJECTs. All of your Tasks will be ERASED",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#f06722",
        confirmButtonText: "Remove projects and DELETE DATA!",
        cancelButtonText: "Sorry, I missclicked",
        closeOnConfirm: false
      },
      function(isConfirm) {
        if (isConfirm) {
          var delIds = {
            "delete_projects": {
              "ids": []
            }
          };
          swal("Deleted!", "Your projects has been deleted.", "success");
          for(var i = 0; i < $scope.tableData.length; i++){
            if($scope.tableData[i].selectedCell === true){
              delIds.delete_projects.ids.push($scope.tableData[i].id);
            }
          }
          $http({
            method  : 'POST',
            url     : apiUrl + 'delete_projects',
            data    : $.param(delIds),
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded'
            } // set the headers so angular passing info as form data (not request payload)
          })
          .then(function successCallback(response) {

          }, function errorCallback(response) {
            swal("Oops!", "Please check your internet connection.", "warning");
          })
          .finally(function() {
            $scope.tableData = [];
            $scope.availableCompanies = [];
            $scope.loadingInit();
          });
        }
      });
    } else {
      swal("Oops!", "Please choose at least ONE item to delete", "warning");
    }
  }
  
  // sort table
  $scope.order = function(predicate) {
    // $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
    if ($scope.predicate === predicate) {
      $scope.reverse = !$scope.reverse;
      // $scope.tableData.reverse();
    } else {
      $scope.reverse = false;
      // if (predicate === 'projectName') {
      //   $scope.tableData.sort(function(a, b) {
      //     var nameA = a.projectName.toLowerCase(),
      //         nameB = b.projectName.toLowerCase()
      //     if (nameA < nameB) //sort string ascending
      //         return -1
      //     if (nameA > nameB)
      //         return 1
      //     return 0 //default return value (no sorting)
      //   });
      // }
    }
    $scope.predicate = predicate;
  };
}]);
