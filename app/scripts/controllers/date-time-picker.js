'use strict';
// Con bug phan` click resume nhung chua disable checkbox
/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:DateTimePickerCtrl
 * @description
 * # DateTimePickerCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

// Config for POST data cross domain
app.config(function($routeProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});

app.controller('DateTimePickerCtrl', [
  '$scope', '$timeout', '$location', '$http', 'localStorageService', 'getDataServices', '$state',
  function ($scope, $timeout, $location, $http, localStorageService, getDataServices, $state) { 
    
  var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
  // var apiUrl = 'http://192.168.1.104:3000/api/'; // Tri
  // var apiUrl = 'http://192.168.1.126:3000/api/';   // Thuan

  // get data from server
  var retrievedData;
  var sendingData1 = {
    "get_tasks": {
      "auth_token":""
    }
  };
  var sendingData2 = {
    "get_companies": {
      "auth_token":""
    }
  };
  var sendingData3 = {
    "get_notification":{
      "auth_token":""
    }
  };
  var invitationListLength;
  $scope.loadingInit = function() {
    var currentUser = localStorageService.get('currentUser');
    if (!currentUser) {
      $state.go('main');
    } else {
      $scope.pageTitle = "Timer";
      $scope.timerPromise = $http.get('http://httpbin.org/delay/31');
      sendingData1.get_tasks.auth_token = currentUser.data.auth_token;
      sendingData3.get_notification.auth_token = currentUser.data.auth_token;

      // Get Invitation
      getDataServices.getAllData(apiUrl, 'send_notification_for_inviting', sendingData3).then(
        function (result) {
          if(result.hasOwnProperty('message')){
            console.log(result.message);
          }else{
            $('#myModal').modal('show');
            var invitation = {
              "company_name":"",
              "email":"",
              "id":""
            };
            $scope.invitationList = result;
            invitationListLength = $scope.invitationList.length;
          }
        },
        function (error) {
          // handle errors here
          console.log(error.statusText);
          swal("Oops!", "Please check your internet connection.", "warning");
        }
      );
      // Load Table Data
      loadTableDataByTimeEntry();
    }   
  }

  // Setup time retrievingData to add to tableData
  var tmpStart, tmpEnd, tmpStartTime, tmpEndTime, tmpDurationDays, tmpDuration;
  var timeRetrievingDataSetup = function(data){
    tmpStart = data.start;
    tmpEnd = data.end;
    tmpStartTime = moment(tmpStart).format("MM/DD/YYYY HH:mm:ss");
    tmpEndTime = moment(tmpEnd).format("MM/DD/YYYY HH:mm:ss");
    // Get Duration Time
    tmpDurationDays = moment(tmpEndTime, "MM/DD/YYYY HH:mm:ss").diff(moment(tmpStartTime, "MM/DD/YYYY HH:mm:ss"));
    if(tmpDurationDays<60000) tmpDuration = moment.duration(tmpDurationDays,"ms").format("HH:mm:ss")+"s";
    else if((60000<=tmpDurationDays) && (tmpDurationDays<3600000)) tmpDuration = "00:"+moment.duration(tmpDurationDays,"ms").format("HH:mm:ss");
    else tmpDuration = moment.duration(tmpDurationDays,"ms").format("HH:mm:ss");
  }
  var loadTableDataByTimeEntry = function(){
     // Load tasks
      getDataServices.getAllData(apiUrl, 'task_timeentries_are_sorted', sendingData1).then(
        function (result) {
          console.log(result);
          $scope.projectsArray = [];
          $scope.tasksArray = [];
          $scope.timeEntryIdArray = [];
          for(var i=0; i < result.projects.length; i++){
            $scope.projectsArray.push(result.projects[i].id);
          }
          for(var i=0; i < result.tasks.length; i++){
            $scope.tasksArray.push(result.tasks[i].id);
          }
          for(var i=0; i < result.time_entries.length; i++){
            $scope.timeEntryIdArray.unshift(result.time_entries[i].id);
          }
          console.log($scope.timeEntryIdArray);
          for(var i=0; i < result.time_entries.length; i++){
            // Get Start Time and End Time
            var tmp = result.time_entries[i];
            // Setup time retrievingData to add to tableData
            timeRetrievingDataSetup(tmp);
            // Get Index of task
            var taskIndex = $.inArray(tmp.task_id, $scope.tasksArray);
            // Get Index of project
            var projectIndex = $.inArray(result.tasks[taskIndex].project_id, $scope.projectsArray);
            // Display Tasks of project
            if(projectIndex == -1){
              addTask(result.tasks[taskIndex].id, result.tasks[taskIndex].task_name, result.tasks[taskIndex].project_id, "", "", tmp.id, tmpDuration, tmpStart, tmpEnd, "white");
            }
            else{
              addTask(result.tasks[taskIndex].id, result.tasks[taskIndex].task_name, result.tasks[taskIndex].project_id, result.projects[projectIndex].project_name, " . "+result.projects[projectIndex].client_name, tmp.id, tmpDuration, tmpStart, tmpEnd, result.projects[projectIndex].color); 
            }
          }      
        },
        function (error) {
          // handle errors here
          console.log(error.statusText);
        }
      ).finally(function() {
        getProject();
        $scope.timerPromise = null;
        // showFollowDays();
        // Continue previous tag when refresh page
        for(var i=0; i < $scope.tableData.length; i++){
          if($scope.tableData[i].stopTime === null){
            console.log($scope.tableData[i]);
            tmpNewTask.time_entry.id = $scope.tableData[i].timeEntryId;
            tmpNewTask.task.id = $scope.tableData[i].id;
            tmpNewTask.task.project_id = $scope.tableData[i].projectId;
            $scope.taskContent = $scope.tableData[i].task;
            if($scope.tableData[i].project === ""){
              $scope.projectBtnName = "+ Project";
            }else{
              $scope.projectBtnName = $scope.tableData[i].project;
            }
            setColorProjectBtn($scope.tableData[i].color);
            // Get Start Time and End Time
            var tmpStartTime = moment($scope.tableData[i].startTime).format("MM/DD/YYYY HH:mm:ss");
            var tmpEndTime = moment().format("MM/DD/YYYY HH:mm:ss");
            // Get Duration Time
            var tmpTableDurationDays = moment(tmpEndTime, "MM/DD/YYYY HH:mm:ss").diff(moment(tmpStartTime, "MM/DD/YYYY HH:mm:ss"));
            $scope.tmpSeconds = tmpTableDurationDays / 1000;
            $scope.runClock = setInterval($scope.displayTime, 1000);
            $('#stop').show();
            $('#start').hide();
            $scope.tableData.splice(i, 1);
            $scope.timeEntryIdArray.splice(i,1);
          }
        } 
      });   
  }
  
  $scope.confirmInvitation = function(index, companyId, answer){
    $('#notification-'+index).toggle('slow');
    invitationListLength--;
    if(answer == 1){
      swal({
          title: "Joined!",
          text: "",
          type: "success",
          timer: 1500,
          showConfirmButton: false
        });
    }else{
      swal({
          title: "Declined!",
          text: "",
          type: "error",
          timer: 1500,
          showConfirmButton: false
        });
    }
    var currentUser = localStorageService.get('currentUser');
    var confirmInvitation = {
      "answer_of_team_member":{  
        "auth_token":"",
        "company_id":"",
        "answer":""
      }
    }
    confirmInvitation.answer_of_team_member.auth_token = currentUser.data.auth_token;
    confirmInvitation.answer_of_team_member.company_id = companyId;
    confirmInvitation.answer_of_team_member.answer= answer; console.log(confirmInvitation);
    $http({
        method: 'POST',
        url: apiUrl + 'get_answer_from_notification',
        data: $.param(confirmInvitation), // pass in data as strings
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        } // set the headers so angular passing info as form data (not request payload)
    })
    .then(function successCallback(response) {
        console.log(response);
    }, function errorCallback(response) {
        console.log('fail');
        console.log(response);
        swal("Oops!", "Please check your internet connection.", "warning");
    }).finally(function(){
      if(invitationListLength == 0){
        $('#myModal').modal('hide');
      }
    });
  }

  $scope.newProject = function() {
    $state.go('new-project');
  }

  // var getProject = function(){
  //   // showFollowDays();
  //   // Get project from server
  //     var currentUser = localStorageService.get('currentUser');
  //     sendingData2.get_companies.auth_token = currentUser.data.auth_token;
  //     getDataServices.getAllData(apiUrl,'projects_of_user', sendingData2).then(
  //       function (result) {
  //         console.log(result);
  //         // get owned company data
  //         var tmpAvailableProject = {
  //           "companyName":"",
  //           "projects":[]
  //         };
  //         var tmpProject = {
  //           "projectId": "",
  //           "projectName":"",
  //           "projectColor":""
  //         };
  //         $scope.availableProjects = [];

  //         for (var i = 0; i < result.companies_own.length; i++) {
  //           tmpAvailableProject.companyName = result.companies_own[i].company_name;
  //           for (var j = 0; j < result.companies_own[i].projects.length; j++) {
  //             // get project data of companies_own
  //             var tmp = result.companies_own[i].projects[j];
  //             tmpProject.projectId = tmp.id;
  //             tmpProject.projectName = tmp.project_name;
  //             tmpProject.projectColor = tmp.color;
  //             tmpAvailableProject.projects.push(tmpProject);
  //             tmpProject = {
  //               "projectId":"",
  //               "projectName":"",
  //               "projectColor":""
  //             };
  //           };
  //           if(tmpAvailableProject.projects.length != 0){
  //             $scope.availableProjects.push(tmpAvailableProject);
  //           }
  //           tmpAvailableProject = {
  //             "companyName":"",
  //             "projects":[]
  //           };
  //         };

  //         for (var i = 0; i < result.all_companies.length; i++) {
  //           tmpAvailableProject.companyName = result.all_companies[i].company_name;
  //           for (var j = 0; j < result.all_companies[i].projects.length; j++) {
  //             // get project data of all_companies
  //             var tmp = result.all_companies[i].projects[j];
  //             tmpProject.projectId = tmp.id;
  //             tmpProject.projectName = tmp.project_name;
  //             tmpProject.projectColor = tmp.color;
  //             tmpAvailableProject.projects.push(tmpProject);
  //             tmpProject = {
  //               "projectId":"",
  //               "projectName":"",
  //               "projectColor":""
  //             };
  //           };
  //           if(tmpAvailableProject.projects.length != 0){
  //             $scope.availableProjects.push(tmpAvailableProject);
  //           }
  //           tmpAvailableProject = {
  //             "companyName":"",
  //             "projects":[]
  //           };
  //         };
  //         console.log($scope.availableProjects);

  //       },
  //       function (error) {
  //         // handle errors here
  //         console.log(error.statusText); 
  //       }
  //     );
  // }

  var getProject = function(){
    // showFollowDays();
    // Get project from server
      var currentUser = localStorageService.get('currentUser');
      sendingData2.get_companies.auth_token = currentUser.data.auth_token;
      getDataServices.getAllData(apiUrl,'projects_of_user1', sendingData2).then(
        function (result) {
          console.log(result);
          // get owned company data
          var tmpAvailableProject = {
            "companyName":"",
            "projects":[]
          };
          var tmpProject = {
            "projectId": "",
            "projectName":"",
            "projectColor":""
          };
          $scope.availableProjects = [];

          for (var i = 0; i < result.projects.length; i++) {
            tmpAvailableProject.companyName = result.projects[i].company.name;
            for (var j = 0; j < result.projects[i].company.projects.length; j++) {
              // get project data of companies_own
              var tmp = result.projects[i].company.projects[j];
              tmpProject.projectId = tmp.id;
              tmpProject.projectName = tmp.project_name;
              tmpProject.projectColor = tmp.color;
              tmpAvailableProject.projects.push(tmpProject);
              tmpProject = {
                "projectId":"",
                "projectName":"",
                "projectColor":""
              };
            };
            if(tmpAvailableProject.projects.length != 0){
              $scope.availableProjects.push(tmpAvailableProject);
            }
            tmpAvailableProject = {
              "companyName":"",
              "projects":[]
            };
          };
          console.log($scope.availableProjects);
        },
        function (error) {
          // handle errors here
          console.log(error.statusText); 
        }
      );
  }

  // Handle datetime-picker
  // Prototype if a string contains only numbers
  String.prototype.isNumber = function(){
    return /^\d+$/.test(this);
  }
  $scope.dateTimePicker = function(){
    $scope.dateTimeNow();
    $scope.taskTime = "00:00:00";
  }
  $scope.dateTimeNow = function() {
    $scope.date = new Date();
  };
  $scope.dateTimeNow();
  $scope.toggleMinDate = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.maxDate = new Date('2014-06-22');
  $scope.toggleMinDate();
  $scope.dateOptions = {
    startingDay: 1,
    showWeeks: false
  };
  // Disable weekend selection
  $scope.disabled = function(calendarDate, mode) {
    return mode === 'day' && ( calendarDate.getDay() === 0 || calendarDate.getDay() === 6 );
  };
  $scope.hourStep = 1;
  $scope.minuteStep = 15;
  $scope.timeOptions = {
    hourStep: [1, 2, 3],
    minuteStep: [1, 5, 10, 15, 25, 30]
  };
  $scope.showMeridian = true;
  $scope.timeToggleMode = function() {
    $scope.showMeridian = !$scope.showMeridian;
  };
  $scope.$watch("date", function(value) {
    console.log('New date value:' + value);
  }, true);
  $scope.resetHours = function() {
    $scope.date.setHours(1);
  };

  //Handle UI when executing the functions
  $(document).click(function(){
    $('#project-container').hide();
    $('#time-container').hide();
  });
  $('#project-btn').on('click',function(event){
    event.stopPropagation();
    event.preventDefault();
    $('#project-container').toggle();
  });
  $('#project-container').on('click', function(){
    event.stopPropagation();
    event.preventDefault();
  });
  $('#time-container').on('click', function(){
    event.stopPropagation();
    event.preventDefault();
  });
  $('#datetime-picker').on('click', function(event){
    event.stopPropagation();
    event.preventDefault();
    $(this).select();
    $('#time-container').toggle();
  });
  $('#datetimepicker-done-btn').on('click', function(){
    $('#time-container').toggle();
  });
  $('#datetimepicker-cancel-btn').on('click', function(){
    $('#time-container').toggle();
  });

  /*Variaties Declaration*/
  var $stopwatch = {
      container: document.getElementById('datetime-picker')
  };
  $scope.taskTime = "00:00:00";
  $scope.counter = 0;
  $scope.tmpSeconds = 0;

  // display counter time at timer
  $scope.displayTime = function() {
    // console.log(moment());
    $stopwatch.container.value = moment().hour(0).minute(0).second($scope.tmpSeconds + $scope.counter++).format('HH:mm:ss');
  }

  // function to start the counter
  $scope.projectId = null;
  $scope.startWatch = function() {
    var tmpStart;
    if($scope.taskTime === "00:00:00"){
      $('#stop').show();
      $('#start').hide();
      // Disable stop button for 1 seconds
        $('#stop').prop('disabled',true);
        setTimeout(function() {
          $('#stop').prop('disabled',false);
        },1000);   // enable after 1 seconds
      tmpStart = moment().format();
      $scope.runClock = setInterval($scope.displayTime, 1000);
      addNewTaskStart($scope.taskContent, $scope.projectId, tmpStart);
    }
    else{
      if($scope.taskTime.isNumber() && ($scope.taskTime <= 43200)){
        tmpStart = moment($scope.date).format();
        addNewTaskStart($scope.taskContent, $scope.projectId, tmpStart, true);
        // Disable start button for 2 seconds
        $('#start').prop('disabled',true);
        setTimeout(function() {
          $('#start').prop('disabled',false);
        },2000);   // enable after 2 seconds
      }
      else{
        if($scope.taskTime > 43200){
          $scope.taskTime = "00:00:00";
          swal({
            title: "Failure!",
            text: "You can't set the time because It's over 1 month.",
            type: "error",
            showConfirmButton: true,
            closeOnConfirm: true
          });
        }
        else{
          $scope.taskTime = "00:00:00";
          swal({
              title: "Failure!",
              text: "You can't set the time because It contains letters.",
              type: "error",
              showConfirmButton: true,
              closeOnConfirm: true
          });
        }
      }
    }
  }

  // function to stop the counter
  $scope.stopWatch = function () {
    $('#start').show();
    $('#stop').hide();
    var tmpStop = moment().format();
    $stopwatch.container.value = "00:00:00";
    clearInterval($scope.runClock);
    $scope.counter = 0;
    // Set para for addTask
    addNewTaskStop($scope.taskContent, tmpStop);
    for(var i=0; i < $scope.tableData.length; i++){
      uiStoppingSetup(i);
    }  
  }

  /*Time Table*/
  $scope.tableData = [];
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  // add task to the table
  var addTask = function(taskId, taskContent, projectId, projectName, clientName, timeEntryId, taskTime, startTime, stopTime, projectColor){
    if(taskContent === "") 
      taskContent = "(no description)";
    var tmpDate, tmpStartDate, tmpStopDate, tmpStartMonth, tmpStopMonth;
    tmpStartDate = moment(startTime).format("MM/DD/YYYY");
    tmpStopDate = moment(stopTime).format("MM/DD/YYYY");
    tmpStartMonth = moment(startTime).month();
    tmpStopMonth = moment(stopTime).month();
    if(tmpStartDate === tmpStopDate){
      tmpDate = months[tmpStartMonth]+", "+moment(tmpStartDate).get('date');
    }else{
      if(tmpStartMonth === tmpStopMonth){
        tmpDate = months[tmpStartMonth]+", "+moment(tmpStartDate).get('date')+" - "+moment(tmpStopDate).get('date');
      }else{
        tmpDate = months[tmpStartMonth]+", "+moment(tmpStartDate).get('date')+" - "+months[tmpStopMonth]+", "+moment(tmpStopDate).get('date');
      }
      
    }
    projectName = projectName.toUpperCase();
    $scope.taskData = {
      task: taskContent, 
      project: projectName,
      client: clientName,
      duration: taskTime, 
      date: tmpDate,
      timeInterval: moment(startTime).format("HH:mm:ss") + ' - ' + moment(stopTime).format("HH:mm:ss")
    };
    $scope.taskData.startTime = startTime;
    $scope.taskData.stopTime = stopTime;
    $scope.taskData.id = taskId;
    $scope.taskData.projectId = projectId;
    $scope.taskData.timeEntryId = timeEntryId;
    if(projectColor === "white"){
      $scope.taskData.color = projectColor;
    }
    else{
       $scope.taskData.color = projectColor + "; color: white;";
    }
    $scope.tableData.unshift($scope.taskData);

    //set to Default Value
    $scope.projectBtnName = "+ Project";
    $scope.projectColor = "white";
    setColorProjectBtn("white");
    $scope.taskContent = "";
    $scope.clientName = "";
    $scope.projectName = "";
    $scope.projectId = null;
  }

  var tmpNewTask = { 
      "task":{  
        "id":"",
        "project_id":"",
        "task_name":""
      },
      "time_entry":{  
        "id":"",
        "task_id":"",
        "start":"",
        "end":""
      }
    };
  var addNewTaskStart = function(taskContent, projectId, startTime, datetimeSelected) {
    // get current user's id
    var currentUser = localStorageService.get('currentUser');

    // create a 'new company' object
    var newTaskStart = {
      "start_task_time_entry":{  
        "auth_token":"",
        "start":"",
        "task":{  
           "project_id":"",
           "task_name":""
        }
      }
    };
    newTaskStart.start_task_time_entry.auth_token = currentUser.data.auth_token;
    newTaskStart.start_task_time_entry.task.task_name = taskContent;
    newTaskStart.start_task_time_entry.task.project_id = projectId;
    newTaskStart.start_task_time_entry.start = startTime;
    $http({
            method: 'POST',
            url: apiUrl + 'start_task_time_entry',
            data: $.param(newTaskStart), // pass in data as strings
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            } // set the headers so angular passing info as form data (not request payload)
        })
        .then(function successCallback(response) {
          // Setup retrievingData from server for var tmpNewTask when creating a New Task
          tmpNewTask.task.id = response.data.task.id;
          tmpNewTask.task.project_id = response.data.task.project_id;
          tmpNewTask.task.task_name = response.data.task.task_name;
          tmpNewTask.time_entry.id = response.data.time_entry.id;
          tmpNewTask.time_entry.task_id = response.data.time_entry.task_id;
          tmpNewTask.time_entry.start = response.data.time_entry.start;
          tmpNewTask.time_entry.end = response.data.time_entry.end;
          console.log(tmpNewTask);
        }, function errorCallback(response) {
            console.log('fail');
            console.log(response);
            swal("Oops!", "Please check your internet connection.", "warning");
        }).finally(function(){
          if(datetimeSelected == true){
            var tmpAfterAdding = moment($scope.date).add($scope.taskTime, "minutes");
            var tmpStop = moment(tmpAfterAdding).format();
            addNewTaskStop($scope.taskContent, tmpStop);
            $scope.dateTimeNow();
            $scope.taskTime = "00:00:00";
            setColorProjectBtn("white");
          }
        });
  }

  var addNewTaskStop = function(taskContent, stopTime){
    $scope.timerPromise = $http.get('http://httpbin.org/delay/31');
    // get current user's id
    var currentUser = localStorageService.get('currentUser');
    $scope.tmpSeconds = 0; 
    $scope.timeEntryIdArray.unshift(tmpNewTask.time_entry.id);
    // create a 'new company' object
    var newTaskStop = {
      "stop_task_time_entry":{  
        "timeentry_id":"",
        "end":"",
        "task_id":"",
        "task":{  
           "project_id":"",
           "task_name":""
        }
      }
    };
    newTaskStop.stop_task_time_entry.timeentry_id = tmpNewTask.time_entry.id;
    newTaskStop.stop_task_time_entry.end = stopTime;
    newTaskStop.stop_task_time_entry.task_id = tmpNewTask.task.id;
    newTaskStop.stop_task_time_entry.task.project_id = tmpNewTask.task.project_id;
    newTaskStop.stop_task_time_entry.task.task_name = taskContent;

    $http({
      method: 'POST',
      url: apiUrl + 'stop_task_time_entry',
      data: $.param(newTaskStop), // pass in data as strings
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      } // set the headers so angular passing info as form data (not request payload)
    })
    .then(function successCallback(response) {
      console.log(response);
      var tmp = response.data;
      // Setup time retrievingData to add to tableData
      timeRetrievingDataSetup(tmp.time_entry);
      // Setup projectName, projectClient, projectColor
      var projectName, projectClient, projectColor;
      if (tmp.task.project_id === null) {
        projectName = "";
        projectClient = "";
        projectColor = "white";
      } else {
        projectName = tmp.project.project_name;
        projectClient = " . " + tmp.project.client_name;
        projectColor = tmp.project.color;
      }
      addTask(tmp.task.id, tmp.task.task_name, tmp.task.project_id, projectName, projectClient, tmp.time_entry.id, tmpDuration, tmpStart, tmpEnd, projectColor);
    }, function errorCallback(response) {
      console.log('fail');
      console.log(response);
      swal("Oops!", "Please check your internet connection.", "warning");
    })
    .finally(function(){
      $scope.timerPromise = null;
    });
  }

  // Set projectColor
  var setColorProjectBtn = function(color){
    if(color === "default" || color === "white"){
      color = "#f06722";
    }
    $('#project-btn').css("background-color",color);
  }

  // Set projectName
  $scope.projectBtnName = "+ Project";
  $scope.setProjectName = function(projectId, projectName, projectColor){
    setColorProjectBtn(projectColor);
    if(projectName === ""){
      $scope.projectBtnName = "+ Project";
    }
    else{
      $scope.projectBtnName = projectName;
    }
    $scope.projectId = projectId;
    $scope.projectName = projectName;
    $("#project-container").hide();
  }

  // checkbox for select all in timer page
  var checked = false;
  $scope.selectAll = function() {
    for(var i=0; i<$scope.tableData.length; i++){
      if($('#checkBox-'+i).prop("disabled") != true){
        $scope.tableData[i].selectedCell = !checked;
      }
    }
    checked = !checked;
  }
  // resume a task
  $scope.resumeTask = function(taskId, timeEntryId) {
    console.log("index: "+index+"===> taskId: "+taskId+"===> timeEntryId: "+timeEntryId);
    console.log($scope.timeEntryIdArray);
    console.log($scope.tableData);
    // Get index of task in tableData
    var index = $.inArray(timeEntryId, $scope.timeEntryIdArray);
    console.log("index ====>"+index);
    $('#stop').show();
    $('#start').hide();
    // Show resumeBtn and hide pauseBtn before
    for(var i=0; i < $scope.tableData.length; i++){
      if(i != index){
        $("#resumeBtn-"+i).show();
        $("#pauseBtn-"+i).hide();
      }
    }
   // Disable pause button for 1 seconds
    $("#pauseBtn-"+index).addClass("disabled");
    setTimeout(function() {
      $("#pauseBtn-"+index).removeClass("disabled"); 
    },1000);   // enable after 1 seconds
    var resumeTask = {
      "resume_task_time_entry":{
        "start":"",
        "task_id":""
      }
    };
    // Add task which is runing
    $scope.taskTime = $stopwatch.container.value;
    if($scope.taskTime !== "00:00:00"){
      var index1 = index + 1;
      // Setup UI When Resume a task
      $('#current-selected-'+index1).css("background-color","#f0f090");
      $("#removeBtn-"+index1).addClass("disabled");
      $("#removeBtn-"+index1).removeClass("enabled");
      $('#checkBox-'+index1).prop("disabled",true);
      $scope.tableData[index1].selectedCell = false;
      $("#resumeBtn-"+index1).hide();
      $("#pauseBtn-"+index1).show();
      for(var i = 0; i < $scope.tableData.length; i++){
        if(i !== index1){
          $('#current-selected-'+i).css("background-color","white");
          $("#removeBtn-"+i).addClass("enabled");
          $("#removeBtn-"+i).removeClass("disabled"); 
        }
      }
      var tmpStop = moment().format();
      addNewTaskStop(tmpNewTask.task.task_name, tmpStop);
      resumeTask.resume_task_time_entry.start = moment().format();
      resumeTask.resume_task_time_entry.task_id = taskId;
      console.log(resumeTask);
      $http({
          method: 'POST',
          url: apiUrl + 'resume_task_time_entry',
          data: $.param(resumeTask), // pass in data as strings
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          } // set the headers so angular passing info as form data (not request payload)
      })
      .then(function successCallback(response) {
        // Setup retrievingData from server for var tmpNewTask when creating a New Task
        tmpNewTask.task.id = response.data.task.id;
        tmpNewTask.task.project_id = response.data.task.project_id;
        tmpNewTask.task.task_name = response.data.task.task_name;
        tmpNewTask.time_entry.id = response.data.time_entry.id;
        tmpNewTask.time_entry.task_id = response.data.time_entry.task_id;
        tmpNewTask.time_entry.start = response.data.time_entry.start;
        tmpNewTask.time_entry.end = response.data.time_entry.end;
        // set taskContent and projectBtnName
        $scope.taskContent = $scope.tableData[index1].task;
        if($scope.tableData[index1].project === ""){
          $scope.projectBtnName = "+ Project";
        }else{
          $scope.projectBtnName = $scope.tableData[index1].project;
        }
        setColorProjectBtn($scope.tableData[index1].color);
      }, function errorCallback(response) {
          console.log('fail');
          console.log(response);
          swal("Oops!", "Please check your internet connection.", "warning");
      });
    }
    else{
      // Setup UI When Resume a task
      $('#current-selected-'+index).css("background-color","#f0f090");
      $("#removeBtn-"+index).addClass("disabled");
      $("#removeBtn-"+index).removeClass("enabled");
      $('#checkBox-'+index).prop("disabled",true);
      $scope.tableData[index].selectedCell = false;
      $("#resumeBtn-"+index).hide();
      $("#pauseBtn-"+index).show();
      // set taskContent and projectBtnName
        $scope.taskContent = $scope.tableData[index].task;
        if($scope.tableData[index].project === ""){
          $scope.projectBtnName = "+ Project";
        }else{
          $scope.projectBtnName = $scope.tableData[index].project;
        }
        setColorProjectBtn($scope.tableData[index].color);   
      resumeTask.resume_task_time_entry.start = moment().format();
      resumeTask.resume_task_time_entry.task_id = taskId;
      $http({
          method: 'POST',
          url: apiUrl + 'resume_task_time_entry',
          data: $.param(resumeTask), // pass in data as strings
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          } // set the headers so angular passing info as form data (not request payload)
      })
      .then(function successCallback(response) {
        console.log(response.data);
        tmpNewTask = { 
          "task":{  
            "id":"",
            "project_id":"",
            "task_name":""
          },
          "time_entry":{  
            "id":"",
            "task_id":"",
            "start":"",
            "end":""
          }
        };
        // Setup retrievingData from server for var tmpNewTask when creating a New Task
        tmpNewTask.task.id = response.data.task.id;
        tmpNewTask.task.project_id = response.data.task.project_id;
        tmpNewTask.task.task_name = response.data.task.task_name;
        tmpNewTask.time_entry.id = response.data.time_entry.id;
        tmpNewTask.time_entry.task_id = response.data.time_entry.task_id;
        tmpNewTask.time_entry.start = response.data.time_entry.start;
        tmpNewTask.time_entry.end = response.data.time_entry.end;      
      }, function errorCallback(response) {
          console.log('fail');
          console.log(response);
          swal("Oops!", "Please check your internet connection.", "warning");
      });
    }
    clearInterval($scope.runClock);
    $scope.counter = 0;
    $scope.runClock = setInterval($scope.displayTime, 1000);
  }

  // Setup UI when Stop
  var uiStoppingSetup = function(index){
    $("#removeBtn-"+index).addClass("enabled");
    $("#removeBtn-"+index).removeClass("disabled");
    setColorProjectBtn("default");
    $("#resumeBtn-"+index).show();
    $("#pauseBtn-"+index).hide();
    $('#current-selected-'+index).css("background-color","white");
    $('#checkBox-'+index).prop("disabled",false); 
  }
  $scope.pauseTask = function(timeEntryId){
    var index = $.inArray(timeEntryId, $scope.timeEntryIdArray);
    $('#start').show();
    $('#stop').hide();
    uiStoppingSetup(index);
    // Set value for resuming task
    $stopwatch.container.value = "00:00:00";
    clearInterval($scope.runClock);
    $scope.counter = 0;
    var tmpStop = moment().format();
    addNewTaskStop($scope.taskContent, tmpStop);
  }
  
  //Load tableData again and clear Interval Clock
  var uiRemovingSetup = function(){
    $scope.tableData = [];
    $scope.taskTime = $stopwatch.container.value;
    if($scope.taskTime !== "00:00:00")
      clearInterval($scope.runClock);
  }
  // remove a task
  $scope.removeTask = function(timeEntryId) {
    var index = $.inArray(timeEntryId, $scope.timeEntryIdArray);
    swal({
      title: "Are you sure?",
      text: "Do you want to delete this task ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#f06722",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "Cancel.",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $scope.timerPromise = $http.get('http://httpbin.org/delay/31');
        swal({
          title: "Deleted!",
          text: "Your tasks has been deleted.",
          type: "success",
          timer: 1500,
          showConfirmButton: false
        });

        var removeTask = {
          "delete_time_entry": {
            "id": ""
          }
        };
        removeTask.delete_time_entry.id = timeEntryId;
        $http({
          method: 'POST',
          url: apiUrl + 'deletetimeentry',
          data: $.param(removeTask), // pass in data as strings
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          } // set the headers so angular passing info as form data (not request payload)
        })
        .then(function successCallback(response) {
          $scope.tableData.splice(index, 1);
          $scope.timeEntryIdArray.splice(index,1);
        }, function errorCallback(response) {
          console.log('fail');
          console.log(response);
          swal("Oops!", "Please check your internet connection.", "warning");
        })
        .finally(function(){
          loadTableDataByTimeEntry();
          uiRemovingSetup();
          $scope.timerPromise = null;
        });
      }
    });
  }

  // remove checked task
  $scope.removeCheckedTask = function(){
    var tmpCheck = false;
    angular.forEach($scope.tableData, function (tdata) {
      if (tdata.selectedCell) {
        tmpCheck = true;
      }
    });

    if (tmpCheck) {
      swal({
        title: "Are you sure?",
        text: "Do you want to delete these tasks ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#f06722",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Cancel.",
        closeOnConfirm: false
      },
      function(isConfirm) {
        if (isConfirm) {
          swal({
            title: "Deleted!",
            text: "Your tasks has been deleted.",
            type: "success",
            timer: 1500,
            showConfirmButton: false
          });
          var removeTaskId = [];
          $scope.toggleInput = false;
          for(var i=0; i < $scope.tableData.length; i++){
            if($scope.tableData[i].selectedCell == true){
              removeTaskId.push($scope.tableData[i].timeEntryId);
              $scope.timeEntryIdArray.splice(i,1);
              $scope.tableData.splice(i,1);
              i--; 
            }
          }
          var removeTasks = {
            "delete_time_entries":{  
              "ids":removeTaskId
            }
          };

          $http({
            method: 'POST',
            url: apiUrl + 'deletetimeentries',
            data: $.param(removeTasks), // pass in data as strings
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            } // set the headers so angular passing info as form data (not request payload)
          })
          .then(function successCallback(response) {
            
          }, function errorCallback(response) {
            console.log('fail');
            console.log(response);
            swal("Oops!", "Please check your internet connection.", "warning");
          })
          .finally(function() {
            $scope.tableData = [];
            loadTableDataByTimeEntry();
            uiRemovingSetup();
          });       
        }
      });
    } else {
      swal("Oops!", "Please choose at least ONE item to delete", "warning");
    }
  }

  // sort table
  $scope.order = function(predicate) { console.log(predicate);
      if (typeof($scope.predicate) === "string") {
        if ($scope.predicate.toLowerCase() === predicate.toLowerCase()) {
          $scope.reverse = !$scope.reverse;
        } else {
          $scope.reverse = false;
        }
      }
      $scope.predicate = predicate;
  };
}]);
