'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:TeamCtrl
 * @description
 * # TeamCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

// Config for POST data cross domain
app.config(function($routeProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});

app.controller('TeamCtrl', [
    '$scope', 'localStorageService', '$state', '$http', 'getDataServices',
    function ($scope, localStorageService, $state, $http, getDataServices) {

    var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
    // var apiUrl = 'http://192.168.1.104:3000/api/';   // Tri
    // var apiUrl = 'http://192.168.1.126:3000/api/';   // Thuan

    $scope.tableData = [
    	// {email: "FakeEmail1@troll.com", department: "Muscian", status: "Verified"},
    	// {email: "FakeEmail2@troll.com", department: "2D Artist", status: "Verified" },
    	// {email: "FakeEmail3@troll.com", department: "3D Artist", status: "Verified"},
    	// {email: "FakeEmail4@troll.com", department: "Programmer", status: "Not Verified" },
    	// {email: "FakeEmail5@troll.com", department: "Story", status: "Verified"}
    ];
    $scope.allCompanies = [];
    var ownedCompanies = [];
    var availableDepartments = [];
    var saveTableData;
    var saveAvailableDepartments;
    var dataCompanyDepartment = {
        "get_departments": {
            "auth_token":""
        }
    };
    var dataTeamMember = {
        "get_team_members": {
            "auth_token":""
        }
    };

    $scope.loadingInit = function() {
        // check for current user is available
        var currentUser = localStorageService.get('currentUser');
        if (!currentUser) {
            $state.go('main');
        } else {
            $scope.pageTitle = "Team Management";
            $scope.teamPromise = $http.get('http://httpbin.org/delay/31');

            // get company and department
            dataCompanyDepartment.get_departments.auth_token = currentUser.data.auth_token;
            getDataServices.getAllData(apiUrl, 'owned_companies_departments', dataCompanyDepartment).then(
                function (result) { 
                    // -------------------- select 2 ----------------------- //
                    for (var i = 0; i < result.owned_companies.length; i++) {
                        var tmpCompany = {
                            "id": "",
                            "text": ""
                        }
                                 
                        tmpCompany.id = result.owned_companies[i].id;
                        tmpCompany.text = result.owned_companies[i].company_name;
                        ownedCompanies.push(tmpCompany);
                    }; 
                    $("#select-company-owned").select2({
                        data: ownedCompanies
                    });
                    for (var i = 0; i < result.owned_departments.length; i++) {
                        var tmpDepartment = {
                            "id": "",
                            "text": "",
                            "company_id": ""
                        }

                        tmpDepartment.id = result.owned_departments[i].id;
                        tmpDepartment.text = result.owned_departments[i].department_name;
                        tmpDepartment.company_id = result.owned_departments[i].company_id;
                        availableDepartments.push(tmpDepartment);
                    };
                    $("#select-department").select2({
                        data: availableDepartments
                    });
                    saveAvailableDepartments = availableDepartments;
                    // ------------------------------------------------------- //

                    getAllCompany(result.owned_companies, true);
                    getAllCompany(result.joined_companies, false);
                },
                function (error) {
                    // console.log(error.statusText);
                    swal("Oops!", "Please check your internet connection.", "warning");
                }
            )
            .finally(function() {
                // get team member
                dataTeamMember.get_team_members.auth_token = currentUser.data.auth_token;
                getDataServices.getAllData(apiUrl, 'members_of_company', dataTeamMember).then(
                    function (result) { 
                        getTeamMember(result.owned_team_members, true);
                        getTeamMember(result.joined_team_members, false);

                        var counter = getNumberOfMember(result.owned_team_members);
                        for (var i = 0; i < $scope.allCompanies.length; i++) {
                            $scope.allCompanies[i].numberOfMember = counter[$scope.allCompanies[i].id];
                        };

                        counter = getNumberOfMember(result.joined_team_members);
                        for (i = 0; i < $scope.allCompanies.length; i++) { 
                            if (typeof($scope.allCompanies[i].numberOfMember) === 'undefined') {
                                $scope.allCompanies[i].numberOfMember = counter[$scope.allCompanies[i].id];
                            }
                        };
                        saveTableData = $scope.tableData;
                    },
                    function (error) {
                        // console.log(error.statusText);
                    }
                ).finally(function() {
                    for (var i = 0; i < $scope.allCompanies.length; i++) { ;
                        if (typeof($scope.allCompanies[i].numberOfMember) === 'undefined') {
                            $scope.allCompanies[i].numberOfMember = 0;
                        }
                    };
                    $scope.teamPromise = null;
                });
            });   
        }
    }

    var getAllCompany = function(company, isOwned) {
        for (var i = 0; i < company.length; i++) {
            var tmpCompany = {
                "id": "",
                "company_name": "",
                "numberOfMember": 0
            }
                     
            tmpCompany.id = company[i].id;
            if (isOwned) {
                tmpCompany.company_name = 'Owned - ' + company[i].company_name;  
            } else {
                tmpCompany.company_name = 'Joined - ' + company[i].company_name;  
            }
            $scope.allCompanies.push(tmpCompany); 
        };
    }

    var getNumberOfMember = function(company) {
        var counter = {};
        // count number of members in a company
        for (var i = 0; i < company.length; i += 1) {
            counter[company[i].company_id] = (counter[company[i].company_id] || 0) + 1;
        } 
        return counter;
    }

    var getTeamMember = function(company, isOwned) {
        for (var i = 0; i < company.length; i++) {
            var tmpTeamMember = {
                "teammember_id": "",
                "email": "",
                "department": "",
                "status": "",
                "company_ad": "",
                "company_admin_id": "",
                "company_id": "",
                "company_name": "",
                "company_owned": false,
                "department_id": ""
            }

            tmpTeamMember.teammember_id = company[i].id;
            tmpTeamMember.email = company[i].email;
            tmpTeamMember.department = (company[i].department_name !== null) ? company[i].department_name : "";
            tmpTeamMember.status = (company[i].verify === 1) ? "Verified" : "Not Verified"; 
            tmpTeamMember.company_ad = (company[i].company_ad === 1) ? "Company Admin" : "User"; 
            tmpTeamMember.company_admin_id = company[i].company_admin_id;
            tmpTeamMember.company_id = company[i].company_id;
            tmpTeamMember.company_name = company[i].company_name;
            tmpTeamMember.company_owned = (isOwned) ? true : false;
            tmpTeamMember.department_id = (company[i].department_id !== null) ? company[i].department_id : "";
            $scope.tableData.push(tmpTeamMember);
        };
    }

    // checkbox for select all in timer page
	var checked = false;
	$scope.selectAll = function() {
        var currentUser = localStorageService.get('currentUser');
	  	angular.forEach($scope.tableData, function (tdata) {
            if (tdata.company_owned && currentUser.data.id === tdata.company_admin_id) {
                tdata.selectedCell = !checked;
            }
	  	});
	  	checked = !checked;
	}

    $scope.selectOne = function(index) {
        var currentUser = localStorageService.get('currentUser');
        for (var i = 0; i < $scope.tableData.length; i++) {
            if ($scope.tableData[i].teammember_id === index) { 
                if ($scope.tableData[i].company_owned && currentUser.data.id === $scope.tableData[i].company_admin_id) {
                    
                } else { 
                    $('#selectedCell-' + index).prop('checked', false);
                }
            }
        };
    }

    $scope.addMember = function() {
        var formData = {
            "invite_member": {
                "email": "",
                "company_id": "",
                "department_id": ""
            }
        }
        formData.invite_member.email = $scope.formAddEditMemberData.memberEmail;
        formData.invite_member.company_id = $("#select-company-owned").val();
        formData.invite_member.department_id = $("#select-department").val();
        
        $http ({
            method  : 'POST',
            url     : apiUrl + 'invite_members',
            data    : $.param(formData), // pass in data as strings
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
        })
        .then(function successCallback(response) {
            // console.log(response);
            if (response.data.message.toLowerCase() === "already invited") {
                swal("Oops!", 'The member is already existed.', 'warning');
            } else {
                swal("Yeah!", 'Your invitation has been successfully sent.', 'success');
            }
        }, function errorCallback(response) {
            swal("Oops!", "Please check your internet connection.", "warning");
            // console.log('fail');
            // console.log(response);
        })
        .finally(function() {
            $scope.allCompanies = [];
            ownedCompanies = [];
            availableDepartments = [];
            $scope.tableData = [];
            $scope.loadingInit();
        });
    }

    $scope.editMember = function(teammember_id) {
        var memberData = {
            "update_team_member": {
                "teammember_id": "",
                "teammember": {
                    "department_id": "",
                    "company_id": ""
                }
            }
        }
        var isAllowedUpdate = true;

        for (var i = 0; i < $scope.tableData.length; i++) {
            if ($scope.tableData[i].teammember_id === teammember_id) {
                memberData.update_team_member.teammember_id = $scope.tableData[i].teammember_id;
                break;
            }
        };
        memberData.update_team_member.teammember.company_id = $("#select-company-owned").val();
        memberData.update_team_member.teammember.department_id = $("#select-department").val();

        for (var i = 0; i < $scope.tableData.length; i++) {
            if ($scope.formAddEditMemberData.memberEmail === $scope.tableData[i].email && 
                parseInt(memberData.update_team_member.teammember.department_id) === $scope.tableData[i].department_id) {
                swal("Oops!", "This member in this department is already existed.", "warning");    
                isAllowedUpdate = false;
                break;
            } 
        };
        
        if (isAllowedUpdate) {
            $http ({
                method  : 'POST',
                url     : apiUrl + 'update_team_member',
                data    : $.param(memberData), // pass in data as strings
                headers : {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
            })
            .then(function successCallback(response) {
                // console.log('success');
                // console.log(response);
            }, function errorCallback(response) {
                swal("Oops!", "Please check your internet connection.", "warning");
                // console.log('fail');
                // console.log(response);
            })
            .finally(function() {
                $scope.allCompanies = [];
                ownedCompanies = [];
                availableDepartments = [];
                $scope.tableData = [];
                $scope.loadingInit();
            });
        }
    }

    $scope.changeCompanyAll = function() { 
        if ($('#view-team-companies #select-company-all').val() === "") {
            $scope.tableData = [];
            if (saveTableData) {
                $scope.tableData = saveTableData;
            }
        } else {
            var index = parseInt($('#view-team-companies #select-company-all').val());
            $scope.tableData = [];
            
            if (saveTableData) {
                for (var i = 0; i < saveTableData.length; i++) {
                    if (saveTableData[i].company_id === index) {
                        $scope.tableData.push(saveTableData[i]);
                    }
                };
            }
        }
    }

    // ------------------------------------ changeCompanyModal ---------------------------------------- //
    var changeCompanyOwned = function() { 
        availableDepartments = [];
        $("#select-department").select2("destroy").find('option').remove().select2();
        var company_id = parseInt($("#select-company-owned").val());
        for (var i = 0; i < saveAvailableDepartments.length; i++) { 
            if (saveAvailableDepartments[i].company_id === company_id) {
                availableDepartments.push(saveAvailableDepartments[i]);
            }
        };
        $("#select-department").select2({
            data: availableDepartments
        });
    };
    $("#select-company-owned").on("change", changeCompanyOwned);
    // -------------------------------------------------------------------------------------- //

    $scope.openModalEditMember = function(index) { 
        var currentUser = localStorageService.get('currentUser');
        $scope.formAddEditMemberData = {};
        $scope.formAddEditMemberData.teammember_id = index;
        $scope.formAddEditMemberData.title = "Edit";
        $scope.formAddEditMemberData.editMember = true;
        for (var i = 0; i < $scope.tableData.length; i++) {
            if ($scope.tableData[i].teammember_id === index) {
                if ($scope.tableData[i].company_owned && currentUser.data.id === $scope.tableData[i].company_admin_id) {
                    $('#add-edit-member').modal('show');
                    $scope.formAddEditMemberData.memberEmail = $scope.tableData[i].email;
                    $("#select-company-owned").val($scope.tableData[i].company_id).trigger("change");
                    $("#select-company-owned").prop("disabled", true);
                    $("#select-department").val($scope.tableData[i].department_id).trigger("change");
                } else {
                    swal("Oops!", "You do not have authorization on this member!", "warning");
                }
            }
        }
    }

    $scope.openModalNewMember = function() {
        var company_id = parseInt($("#select-company-owned").val());
        $scope.formAddEditMemberData = {};
        $scope.formAddEditMemberData.editMember = false;
        $scope.formAddEditMemberData.title = "Add new";
        $scope.formAddEditMemberData.memberEmail = null;
        changeCompanyOwned();
        $("#select-company-owned").prop("disabled", false);
        $("#select-department").val($scope.tableData[company_id].department_id).trigger("change");
    }

    $scope.removeMember = function(index) { 
        var currentUser = localStorageService.get('currentUser');
        for (var i = 0; i < $scope.tableData.length; i++) {
            if ($scope.tableData[i].teammember_id === index) {
                if ($scope.tableData[i].company_owned && currentUser.data.id === $scope.tableData[i].company_admin_id) {
                    swal({
                        title: "Are you sure?",
                        text: "You will also remove this user from any projects that they joined.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#f06722",
                        confirmButtonText: "Remove user!",
                        cancelButtonText: "Sorry, I missclicked",
                        closeOnConfirm: false
                    },
                    function(isConfirm){
                        if (isConfirm) {
                            var delId = {
                                "delete_team_members": {
                                    "ids": []
                                }
                            };
                            swal("Removed!", "A user has been removed.", "success");
                            for (var i = 0; i < $scope.tableData.length; i++) {
                                if ($scope.tableData[i].teammember_id === index) {
                                    delId.delete_team_members.ids.push($scope.tableData[i].teammember_id);
                                    break;
                                }
                            }; 
                            $http({ 
                                method: 'POST',
                                url     : apiUrl + 'delete_team_members',
                                data    : $.param(delId),
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                } // set the headers so angular passing info as form data (not request payload)
                            })
                            .then(function successCallback(response) {

                            }, function errorCallback(response) {
                                swal("Oops!", "Please check your internet connection.", "warning");
                            })
                            .finally(function() {
                                $scope.allCompanies = [];
                                $scope.tableData = [];
                                $scope.loadingInit();
                            });
                        }
                    });
                    // $scope.tableData.splice(i, 1);
                    break;
                } else {
                    swal("Oops!", "You do not have authorization on this member!", "warning");
                }
            }
        };
    }

    $scope.removeAll = function(){
        var currentUser = localStorageService.get('currentUser');
        var tmpCheck = false;
        angular.forEach($scope.tableData, function (tdata) {
            if (tdata.selectedCell) {
                tmpCheck = true;
            }
        });
        if (tmpCheck) {
            swal({
                title: "Are you sure?",
                text: "You will also remove these users from any projects that they joined.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#f06722",
                confirmButtonText: "Remove users!",
                cancelButtonText: "Sorry, I missclicked",
                closeOnConfirm: false
            },
            function(isConfirm) { 
                if (isConfirm) { 
                    var delId = {
                        "delete_team_members": {
                            "ids": []
                        }
                    }; 
                    swal("Removed!", "Those users has been removed.", "success");
                    for (var i = 0; i < $scope.tableData.length; i++) {
                        if ($scope.tableData[i].selectedCell == true) {
                            delId.delete_team_members.ids.push($scope.tableData[i].teammember_id);
                        }
                    }; 
                    $http({ 
                        method: 'POST',
                        url     : apiUrl + 'delete_team_members',
                        data    : $.param(delId),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        } // set the headers so angular passing info as form data (not request payload)
                    })
                    .then(function successCallback(response) {

                    }, function errorCallback(response) {
                        swal("Oops!", "Please check your internet connection.", "warning");
                    })
                    .finally(function() {
                        $scope.allCompanies = [];
                        $scope.tableData = [];
                        $scope.loadingInit();
                    });
                }
            });
        } else {
            swal("Oops!", "Please choose at least ONE item to delete", "warning");
        }
    }

    // sort table
    $scope.order = function(predicate) {
        if ($scope.predicate === predicate) {
            $scope.reverse = !$scope.reverse;
            // $scope.tableData.reverse();
        } else {
            $scope.reverse = false;
            // $scope.tableData.sort();            
        }
        $scope.predicate = predicate;
    };
}]);
