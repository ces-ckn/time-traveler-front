'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the timeTravelerFrontApp
 */
angular.module('timeTravelerFrontApp')
  .controller('AboutCtrl', ['$rootScope',function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  }]);
