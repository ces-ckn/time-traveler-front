'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:ManageUserCtrl
 * @description
 * # ManageUserCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

// Config for POST data cross domain
app.config(function($routeProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});

app.controller('ManageUserCtrl', [
	'$scope', 'getDataServices', 'localStorageService', '$rootScope', '$http',
	function ($scope, getDataServices, localStorageService, $rootScope, $http) {

	var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
    // var apiUrl = 'http://192.168.1.104:3000/api/';	// Tri
    // var apiUrl = 'http://192.168.1.126:3000/api/';	// Thuan

	$scope.tableData = [];
	var saveTableData;

	$scope.loadingInit = function() {
    	// check for current user is available
    	var currentUser = localStorageService.get('currentUser');
    	if (!currentUser || currentUser.data.role === 0) {
    		$state.go('main');
    	} else {
    		$scope.pageTitle = "User Management";
    		$scope.manageUserPromise = $http.get('http://httpbin.org/delay/31');
			$http({
	            method: 'GET',
	            cache: false,
	            url: apiUrl + 'manageusers',
	            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	        })
		    .then(function successCallback(response) {
		      	var result = response.data;
		      	var currentUser = localStorageService.get('currentUser');
		      	$scope.numberOfActive = 0;
		      	$scope.numberOfInactive = 0;
		      	for (var i = 0; i < result.length; i++) {
			      	if (result[i].email !== currentUser.data.email) {
			      		var userData = {
				      		"user_id": "",
				      		"email": "",
				      		"role": "",
				      		"roleDisplay": "",
				      		"status": "",
				      		"last_login": ""
				      	}
		      			userData.email = result[i].email;
			      		userData.user_id = result[i].id;
			      		userData.role = (result[i].role === 1) ? "Admin" : "User";
			      		userData.roleDisplay = (result[i].role === 1) ? true : false;
			      		userData.last_login = moment(result[i].updated_at).format("MM-DD-YYYY HH:mm:ss");
			      		if (result[i].status === 1) {
			      			userData.status = "Active";
			      			$scope.numberOfActive++;
			      		} else {
			      			userData.status = "Inactive";
			      		}
			      		$scope.tableData.push(userData);
			      		$scope.numberOfInactive = $scope.tableData.length - $scope.numberOfActive;
		      		}
		      	};
		    }, function errorCallback(response) {
		      	// console.log('fail');
		      	// console.log(response);
		      	swal("Oops!", "Please check your internet connection.", "warning");
		    })
		    .finally(function() {
		    	$scope.manageUserPromise = null;
		    	saveTableData = $scope.tableData;
		    });
		}
    }

	$scope.setActiveAll = function() {
		var currentUser = localStorageService.get('currentUser');
        var tmpCheck = false;
        angular.forEach($scope.tableData, function (tdata) {
            if (tdata.selectedCell) {
                tmpCheck = true;
            }
        });

        if (tmpCheck) {
			swal({
		        title: "Are you sure?",
		        text: "Do you want to take the action (Activate/Deactivate) on the user(s)",
		        type: "warning",
		        showCancelButton: true,
		        confirmButtonColor: "#f06722",
		        confirmButtonText: "Yes, do it!",
		        cancelButtonText: "Sorry, I missclicked",
		        closeOnConfirm: false
		    },
		    function(isConfirm) {
		        if (isConfirm) {
					var userData = {
						"user": {
							"event": 2,
							"ids": []
						}
					};

		            for (var i = 0; i < $scope.tableData.length; i++) {
		            	if ($scope.tableData[i].roleDisplay === true) {
					    	swal("Oops!", "You do not have authorities on ADMIN account", "warning");
		                } else {
		                	if ($scope.tableData[i].selectedCell == true) {
		                    	userData.user.ids.push($scope.tableData[i].user_id);
		                    	swal("Success!", "Your action has been executed.", "success");
		                	} 
					    }
		            }

					$http({
			            method: 'POST',
			            url: apiUrl + 'manageusers/chooseevent',
			            data: $.param(userData), // pass in data as strings
			            headers: {
			                'Content-Type': 'application/x-www-form-urlencoded'
			            } // set the headers so angular passing info as form data (not request payload)
			        })
			        .then(function successCallback(response) {
			        	// console.log('success');
			            // console.log(response);
			        }, function errorCallback(response) {
			        	swal("Oops!", "Please check your internet connection.", "warning");
			            // console.log('fail');
			            // console.log(response);
			        })
			        .finally(function() {
			        	$scope.tableData = [];
			        	$scope.loadingInit();
			        });
		        }
		    });
		} else {
			swal("Oops!", "Please choose at least ONE item to delete", "warning");
		}
	}

	$scope.setActive = function(index) {
		var actionMessage;
		var actionButtonText;
		var active = true;

		for (var i = 0; i < $scope.tableData.length; i++) {
			if ($scope.tableData[i].user_id === index) {
				if ($scope.tableData[i].roleDisplay === false) {
					if ($scope.tableData[i].status === "Inactive") {
						actionMessage = "Do you want to Re-activate this user? All COMPANIES, PROJECTS, MEMBERS and TASKS of this user will be Activated again.";
						actionButtonText = "Activate this user!";
						active = false;
					} else {
						actionMessage = "Do you want to Deactivate this user? All COMPANIES, PROJECTS, MEMBERS and TASKS of this user will be Deactivated until you re-activate him.";
			 			actionButtonText = "Deactivate this user!";
			 			active = true;
					}

					swal({
			            title: "Are you sure?",
			            text: actionMessage,
			            type: "warning",
			            showCancelButton: true,
			            confirmButtonColor: "#f06722",
			            confirmButtonText: actionButtonText,
			            cancelButtonText: "Sorry, I missclicked",
			            closeOnConfirm: false
			        },
			        function(isConfirm){
			            if (isConfirm) {
							var userData = {
								"user": {
									"event": 2,
									"ids": []
								}
							};

							userData.user.ids.push($scope.tableData[i].user_id);
							if (active) {
								swal("Deactivated!", "User has been Deactivated.", "success");
							} else {
								swal("Activated!", "User has been Activated.", "success");
							}

							$http({
					            method: 'POST',
					            url: apiUrl + 'manageusers/chooseevent',
					            data: $.param(userData), // pass in data as strings
					            headers: {
					                'Content-Type': 'application/x-www-form-urlencoded'
					            } // set the headers so angular passing info as form data (not request payload)
					        })
					        .then(function successCallback(response) {
					        	// console.log('success');
					            // console.log(response);
					        }, function errorCallback(response) {
					        	swal("Oops!", "Please check your internet connection.", "warning");
					            // console.log('fail');
					            // console.log(response);
					        })
					        .finally(function() {
					        	$scope.tableData = [];
					        	$scope.loadingInit();
					        });
			            }
			        });
			    } else {
			    	swal("Oops!", "You do not have authorities on ADMIN account", "warning");
			    }
			    break;
			}
		};
	}

	$scope.changeStatus = function() { 
		if ($('.view-status #select-status').val() == 0) {
            $scope.tableData = [];
            if (saveTableData) {
                for (var i = 0; i < saveTableData.length; i++) {
                    if (saveTableData[i].status === "Inactive") {
                    	$scope.tableData.push(saveTableData[i]);
                    }
                };
            }
        }
        if ($('.view-status #select-status').val() == 1) {
            $scope.tableData = [];
            if (saveTableData) {
                for (var i = 0; i < saveTableData.length; i++) {
                    if (saveTableData[i].status === "Active") {
                    	$scope.tableData.push(saveTableData[i]);
                    }
                };
            }
        }
        if ($('.view-status #select-status').val() == "") {
            $scope.tableData = [];
            if (saveTableData) {
                $scope.tableData = saveTableData;
            }
        }
    }

	// checkbox for select all in timer page
	var checked = false;
	$scope.selectAll = function() {
	  	angular.forEach($scope.tableData, function (tdata) {
	    	tdata.selectedCell = !checked;
	  	});
	  	checked = !checked;
	}

    // sort table
	$scope.order = function(predicate) {
        // $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        if ($scope.predicate === predicate) {
        	$scope.reverse = !$scope.reverse;
        	// $scope.tableData.reverse();
        } else {
        	$scope.reverse = false;
        	$scope.tableData.sort();
    //     	if (predicate === 'email') {
	   //      	$scope.tableData.sort(function(a, b) {
				//     var nameA = a.email.toLowerCase(),
				//         nameB = b.email.toLowerCase()
				//     if (nameA < nameB) //sort string ascending
				//         return -1
				//     if (nameA > nameB)
				//         return 1
				//     return 0 //default return value (no sorting)
				// });
	   //      }
	   //      if (predicate === 'role') {
	   //      	$scope.tableData.sort(function(a, b) {
				//     var nameA = a.role.toLowerCase(),
				//         nameB = b.role.toLowerCase()
				//     if (nameA < nameB) //sort string ascending
				//         return -1
				//     if (nameA > nameB)
				//         return 1
				//     return 0 //default return value (no sorting)
				// });
	   //      }
	   //      if (predicate === 'status') {
	   //      	$scope.tableData.sort(function(a, b) {
				//     var nameA = a.status.toLowerCase(),
				//         nameB = b.status.toLowerCase()
				//     if (nameA < nameB) //sort string ascending
				//         return -1
				//     if (nameA > nameB)
				//         return 1
				//     return 0 //default return value (no sorting)
				// });
	   //      }
        }
        $scope.predicate = predicate;
    };
}]);
