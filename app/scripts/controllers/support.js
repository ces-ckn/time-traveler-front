'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:SupportCtrl
 * @description
 * # SupportCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

// Config for POST data cross domain
app.config(function($routeProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});

app.controller('SupportCtrl', [
	'$scope', 'localStorageService', '$state', '$http', 'getDataServices',
    function ($scope, localStorageService, $state, $http, getDataServices) {
    $scope.answerContent ={};
    $scope.tableData = [];
    $scope.tableData = [
    	{
    		"id": 1,
    		"question": "How to create, resume and delete tasks."
    	},
    	{
    		"id": 2,
    		"question": "How to create, edit and delete companies."
    	},
    	{
    		"id": 3,
    		"question": "How to create, edit and delete projects."
    	},
    	{
    		"id": 4,
    		"question": "How to invite, edit and delete team's members."
    	},
    	{
    		"id": 5,
    		"question": "How to report tasks."
    	},
    	{
    		"id": 6,
    		"question": "How to change your old password."
    	},
    	{
    		"id": 7,
    		"question": "How to get back you password if you forgot it."
    	}
    ];

    $scope.loadingInit = function() {
        $scope.pageTitle = "Support";
    }
    // $scope.answerShow = false;
    $scope.changeQuestion = function(index) {
        for(var i=0; i< $scope.tableData.length; i++){
            if($scope.tableData[i].id === index){
                $('#answer-'+(i+1)).show();
            }else{
                $('#answer-'+(i+1)).hide();
            }
        }

    	$scope.answerContent.clicked = 'clicked';
    }

    // Answer 1
    $scope.availableProjects = [
        {   "name":"Company 1",
            "projects": [
                {"projectName":"Project 1", "projectColor":"#4DC3FF"},
                {"projectName":"Project 2", "projectColor":"#BC85E6"},
                {"projectName":"Project 3", "projectColor":"#DF7BAA"}
            ]
        },
        {   "name":"Company 2",
            "projects": [
                {"projectName":"Project 4", "projectColor":"#F68D38"},
                {"projectName":"Project 6", "projectColor":"#B27636"},
                {"projectName":"Project 8", "projectColor":"#8AB734"}
            ]
        },
        {   "name":"Company 3",
            "projects": [
                {"projectName":"Project 2", "projectColor":"#14A88E"},
                {"projectName":"Project 9", "projectColor":"#268BB5"},
                {"projectName":"Project 14", "projectColor":"#3C6526"}
            ]
        }
    ];

     //Handle UI when executing the functions
      $(document).click(function(){
        $('#project-container').hide();
        $('#time-container').hide();
      });
      $('#project-btn').on('click',function(event){
        event.stopPropagation();
        event.preventDefault();
        $('#project-container').toggle();
      });
      $('#project-container').on('click', function(){
        event.stopPropagation();
        event.preventDefault();
      });
      $('#datetime-picker').on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
        $(this).select();
        $('#time-container').toggle();
        $('#answer1-dialog3').toggle();
      });
        $('#time-container').on('click', function(){
        event.stopPropagation();
        event.preventDefault();
      });
      $('#datetimepicker-done-btn').on('click', function(){
        $('#time-container').toggle();
        $('#answer1-dialog3').toggle();
      });
      $('#datetimepicker-cancel-btn').on('click', function(){
        $('#time-container').toggle();
        $('#answer1-dialog3').toggle();
      });

    // Set projectName
    $scope.projectBtnName = "+ Project";
    $scope.setProjectName = function(projectName, projectColor){
        setColorProjectBtn(projectColor);
        if(projectName === ""){
          $scope.projectBtnName = "+ Project";
        }
        else{
          $scope.projectBtnName = projectName;
        }
        $("#project-container").hide();
    }

    // Set projectColor
    var setColorProjectBtn = function(color){
        if(color === "default" || color === "white"){
          color = "#f06722";
        }
        $('#project-btn').css("background-color",color);
    }

    // Handle datetime-picker
    // Prototype if a string contains only numbers
    String.prototype.isNumber = function(){
        return /^\d+$/.test(this);
    }
    $scope.dateTimePicker = function(){
        $scope.dateTimeNow();
        $scope.taskTime = "00:00:00";
    }
    $scope.dateTimeNow = function() {
        $scope.date = new Date();
    };
    $scope.dateTimeNow();
    $scope.toggleMinDate = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.maxDate = new Date('2014-06-22');
    $scope.toggleMinDate();
    $scope.dateOptions = {
        startingDay: 1,
        showWeeks: false
    };
      // Disable weekend selection
      $scope.disabled = function(calendarDate, mode) {
        return mode === 'day' && ( calendarDate.getDay() === 0 || calendarDate.getDay() === 6 );
    };
    $scope.hourStep = 1;
    $scope.minuteStep = 15;
    $scope.timeOptions = {
        hourStep: [1, 2, 3],
        minuteStep: [1, 5, 10, 15, 25, 30]
    };
    $scope.showMeridian = true;
    $scope.timeToggleMode = function() {
        $scope.showMeridian = !$scope.showMeridian;
    };
    $scope.$watch("date", function(value) {
        console.log('New date value:' + value);
    }, true);
    $scope.resetHours = function() {
        $scope.date.setHours(1);
    };

    // Datetime-picker
    var $stopwatch = {
      container: document.getElementById('datetime-picker')
    };
    var counter = 0;
    // display counter time at timer
    $scope.displayTime = function() {
        // console.log(moment());
        $stopwatch.container.value = moment().hour(0).minute(0).second(counter++).format('HH:mm:ss');
    }
      // function to start the counter
    $scope.taskTime = "00:00:00";
    $scope.startWatch = function() {
        if($scope.taskTime === "00:00:00"){
          $('#stop').show();
          $('#start').hide();
          $scope.runClock = setInterval($scope.displayTime, 1000);
        }
        else{
            if($scope.taskTime > 43200){
              $scope.taskTime = "00:00:00";
              swal({
                title: "Failure!",
                text: "You can't set the time because It's over 1 month.",
                type: "error",
                showConfirmButton: true,
                closeOnConfirm: true
              });
            }
            else{
              $scope.taskTime = "00:00:00";
              swal({
                  title: "Failure!",
                  text: "You can't set the time because It contains letters.",
                  type: "error",
                  showConfirmButton: true,
                  closeOnConfirm: true
              });
            }
        }
    }

    // function to stop the counter
    $scope.stopWatch = function () {
        $('#start').show();
        $('#stop').hide();
        clearInterval($scope.runClock);
        $scope.counter = 0;
        $scope.taskContent = "";
        $scope.projectBtnName = "+ Project"; 
        uiStoppingSetup(1);
    }

    // Set the appearence of dialog
    $('#task-content').hover(function(){
        $('#answer1-dialog1').slideDown('fast');
    },function(){
        $('#answer1-dialog1').slideUp('fast');
    });
    $('#project-btn').hover(function(){
        $('#answer1-dialog2').slideDown('fast');
    },function(){
        $('#answer1-dialog2').slideUp('fast');
    });
    $('#datetime-picker').hover(function(){
        $('#answer1-dialog3').slideDown('fast');
    },function(){
        $('#answer1-dialog3').slideUp('fast');
    });
    $('#start').hover(function(){
        $('#answer1-dialog4').slideDown('fast');
    },function(){
        $('#answer1-dialog4').slideUp('fast');
    });
    $('#resumeBtn-1').hover(function(){
        $('#answer1-dialog5').slideDown('fast');
    },function(){
        $('#answer1-dialog5').slideUp('fast');
    });
    $('#removeBtn-1').hover(function(){
        $('#answer1-dialog6').slideDown('fast');
    },function(){
        $('#answer1-dialog6').slideUp('fast');
    });

    $scope.resumeTask = function() {
        $('#stop').show();
        $('#start').hide();
        // Show resumeBtn and hide pauseBtn before
        $("#resumeBtn-1").show();
        $("#pauseBtn-1").hide();

        // Setup UI When Resume a task
        $('#current-selected-1').css("background-color","#f0f090");
        $("#removeBtn-1").addClass("disabled");
        $("#removeBtn-1").removeClass("enabled");
        $("#resumeBtn-1").hide();
        $("#pauseBtn-1").show();
        // set taskContent and projectBtnName
        $scope.taskContent = "Task 1";
        $scope.projectBtnName = "Project 1";
        setColorProjectBtn("#DF7BAA");   
        clearInterval($scope.runClock);
        $scope.counter = 0;
        $scope.runClock = setInterval($scope.displayTime, 1000);
    }
    // Setup UI when Stop
    var uiStoppingSetup = function(index){
        $("#removeBtn-"+index).addClass("enabled");
        $("#removeBtn-"+index).removeClass("disabled");
        setColorProjectBtn("default");
        $("#resumeBtn-"+index).show();
        $("#pauseBtn-"+index).hide();
        $('#current-selected-'+index).css("background-color","white");
    }
    $scope.pauseTask = function(timeEntryId){
        $('#start').show();
        $('#stop').hide();
        uiStoppingSetup(1);
        // Set value for resuming task
        $scope.taskContent = "";
        $scope.projectBtnName = "+ Project";
        clearInterval($scope.runClock);
        $scope.counter = 0;
    }
    $scope.removeTask = function() {
      swal({
          title: "Are you sure?",
          text: "Do you want to delete this task ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#f06722",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "Cancel.",
          closeOnConfirm: false
      },
      function(isConfirm) {
          if (isConfirm) {
            $scope.timerPromise = $http.get('http://httpbin.org/delay/3');
            swal({
              title: "Deleted!",
              text: "Your tasks has been deleted.",
              type: "success",
              timer: 1500,
              showConfirmButton: false
            });
            $('#current-selected-1').hide();
          };
      });
    }

    // Answer 2
    $('#new-company-btn').hover(function(){
        $('#answer2-dialog1').slideDown('fast');
    },function(){
        $('#answer2-dialog1').slideUp('fast');
    });
    // Answer 3
    $('#new-project-btn').hover(function(){
        $('#answer3-dialog1').slideDown('fast');
    },function(){
        $('#answer3-dialog1').slideUp('fast');
    });
    // Answer 4
    $('#new-teammember-btn').hover(function(){
        $('#answer4-dialog1').slideDown('fast');
    },function(){
        $('#answer4-dialog1').slideUp('fast');
    });
    $scope.images = {
        "answer1Pic1":"../images/answer1-pic1.jpg",
        "answer2Pic1":"../images/answer2-pic1.jpg",
        "answer2Pic2":"../images/answer2-pic2.jpg",
        "answer3Pic1":"../images/answer3-pic1.png",
        "answer3Pic2":"../images/answer3-pic2.jpg",
        "answer4Pic1":"../images/answer4-pic1.jpg",
        "answer4Pic2":"../images/answer4-pic2.jpg",
        "answer5Pic1":"../images/answer5-pic1.jpg",
        "answer6Pic1":"../images/answer6-pic1.png",
        "answer7Pic1":"../images/answer7-pic1.jpg",
        "answer7Pic2":"../images/answer7-pic2.jpg"
    };
}]);
