'use strict';

/**
 * @ngdoc function
 * @name timeTravelerFrontApp.controller:EditProjectCtrl
 * @description
 * # EditProjectCtrl
 * Controller of the timeTravelerFrontApp
 */
var app = angular.module('timeTravelerFrontApp');

app.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

// config for UI select
app.config(function (uiSelectConfig) {
  uiSelectConfig.theme = 'selectize';
  uiSelectConfig.resetSearchInput = true;
  uiSelectConfig.appendToBody = true;
});

// configuration for cross domain
app.config(function ($routeProvider, $httpProvider){
  $httpProvider.defaults.useXDomain = true;
  $httpProvider.defaults.withCredentials = true;
  delete $httpProvider.defaults.headers.common["X-Requested-With"];
  $httpProvider.defaults.headers.common["Accept"] = "application/json";
  $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});

app.controller('EditProjectCtrl', [
  '$scope', '$http', 'shareProperties', 'localStorageService', '$state', 'getDataServices',
  function ($scope, $http, shareProperties, localStorageService, $state, getDataServices) {

	$scope.loading = false;

  // initialize form data variable
  // $scope.availableCompanies = [];
  $scope.formEditProjectData = {};
  $scope.availableClients = [];
  $scope.availableMembers = [];
  $scope.noProjectData = true;
  var projectData;
  var allClients = [];
  var allMembers = [];
  var sendingData = {
    "get_values_of_new_project": {
      "auth_token":""
    }
  };

	var apiUrl = 'https://time-traveler-back.herokuapp.com/api/';
	// var apiUrl = 'http://192.168.1.104:3000/api/';  // Tri
  // var apiUrl = 'http://192.168.1.126:3000/api/';  // Thuan

  $scope.loadingInit = function() {
    var currentUser = localStorageService.get('currentUser');
    if (!currentUser) {
      $state.go('main');
    } else {
      $scope.pageTitle = "Edit Project";
      $scope.editProjectPromise = $http.get('http://httpbin.org/delay/31');

      // get all companies and clients, members
      sendingData.get_values_of_new_project.auth_token = currentUser.data.auth_token; 
      getDataServices.getAllData(apiUrl, 'values_of_new_project', sendingData).then(
        function (result) {
          allClients[1] = result.owned_clients_name;
          allClients[0] = result.joined_clients_name;

          allMembers[0] = result.owned_teammembers;
          allMembers[1] = result.joined_teammembers;

          // get project data from project page
          projectData = shareProperties.getProperty();
          if (typeof(projectData) !== 'undefined') { 
            $scope.noProjectData = false;
            
            // add client to form
            for (var i = 0; i < allClients.length; i++) {
              addClientToForm(allClients[i], projectData.company.id);
            };
            
            // add member to form
            for (var i = 0; i < allMembers.length; i++) {
              addMemberToForm(allMembers[i], projectData.company.id);
            };

            setFormData(projectData); 
            console.log(projectData);
            // console.log($scope.availableClients);
          }

        },
        function (error) {
          // console.log(error.statusText);
          swal("Oops!", "Please check your internet connection.", "warning");
        }
      ).finally(function(){
        $scope.editProjectPromise = null;
      });      

      
    }
  }

  var setFormData = function(data) {
    $scope.formEditProjectData.client_name = [];
    $scope.formEditProjectData.member_email = [];

    $scope.formEditProjectData.id = data.id;
    $scope.formEditProjectData.project_name = data.projectName;
    $scope.formEditProjectData.projectManager = data.projectManager;
    $scope.formEditProjectData.companies = data.company.company_name;
    if (data.client !== "") {
      $scope.formEditProjectData.client_name.push(data.client);
    }
    $scope.formEditProjectData.desc = data.desc;
    $scope.formEditProjectData.project_color = data.color;
    for (var i = 0; i < data.team.length; i++) {
      var tmpMember = {
        "email": "",
        "teammember_id": ""
      }
      if (data.projectManager !== data.team[i].email) {
        tmpMember.email = data.team[i].email;
        tmpMember.teammember_id = data.team[i].teammember_id;

        $scope.formEditProjectData.member_email.push(tmpMember);
      }
    };
    // sending edited data to project page
    shareProperties.setProperty($scope.formEditProjectData);  
  }

  var addClientToForm = function(client, company_id) {
    if (client) {
      for (var i = 0; i < client.length; i++) {
        if (client[i].company_id === company_id && client[i].client_name !== "") {
          $scope.availableClients.push(client[i].client_name);
        }
      };
    }
  }

  var addMemberToForm = function(member, company_id) {
    if (member) {
      for (var i = 0; i < member.length; i++) {
        if (member[i].company_id === company_id && projectData.projectManager !== member[i].email) {
          var tmpMember = {
            "teammember_id": "",
            "email": ""
          }
          tmpMember.teammember_id = member[i].id;
          tmpMember.email = member[i].email;
          $scope.availableMembers.push(tmpMember);
        }
      };
    }
  }

   /*Project  Color Table*/
  $scope.projectColorRow = [
    "#4DC3FF", "#BC85E6", "#DF7BAA", "#F68D38",
    "#B27636", "#8AB734", "#14A88E", "#268BB5",
    "#6668B4", "#A4506C", "#67412C", "#3C6526", 
    "#094558", "#BC2D07", "#999999", "#F4FA58"
  ];
  $scope.setProjectColor = function(colorId){
    $(".edit-project-page #project-color-div").css("background-color", $scope.projectColorRow[colorId]);
    $scope.formEditProjectData.project_color = $scope.projectColorRow[colorId];
    // $scope.projectColor = $scope.projectColorRow[colorId] + "; color: white;";
  }

	$scope.submitFormEditProject = function() {  
    // object to store project info in order to send them to BE
    var projectObject = {
      "update_project": {
        "project_id": "",
        "project": {
          "project_name": "",
          "client_name": "",
          "color": "",
          "desc": ""
        },
        "teammember_ids": []
      }
    }

    // add to project object   
    projectObject.update_project.project_id = projectData.id;
    projectObject.update_project.project.project_name = $scope.formEditProjectData.project_name;
    projectObject.update_project.project.client_name = $scope.formEditProjectData.client_name.toString();
    projectObject.update_project.project.desc = $scope.formEditProjectData.desc;
    projectObject.update_project.project.color = $scope.formEditProjectData.project_color;
    projectObject.update_project.teammember_ids.push(parseInt(projectData.projectManager_id));
    for (var i = 0; i < $scope.formEditProjectData.member_email.length; i++) {
      projectObject.update_project.teammember_ids.push($scope.formEditProjectData.member_email[i].teammember_id);
    };

		$http ({
    	method  : 'POST',
    	url     : apiUrl + 'update_project',
    	data    : $.param(projectObject), // pass in data as strings
    	headers : {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
    })
    .then(function successCallback(response) {
	    $scope.loading = false;
      $state.go('project');
		}, function errorCallback(response) {
      swal("Oops!", "Please check your internet connection.", "warning");
	    $scope.loading = false;
		})
    .finally(function() {
      
    });
	};

  $scope.cancelFormEditProject = function() {
    $state.go('project');
  }
}]);
