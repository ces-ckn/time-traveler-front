'use strict';

/**
 * @ngdoc overview
 * @name timeTravelerFrontApp
 * @description
 * # timeTravelerFrontApp
 *
 * Main module of the application.
 */

var app = angular.module('timeTravelerFrontApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.sortable',
    'LocalStorageModule',
    'ui.router',
    'ui.bootstrap',
    'ngFacebook', 
    'satellizer', 
    'ngMessages',
    'ui.select',
    'ngRoute',
    'ngTagsInput',
    'cgBusy',
    'chart.js',
    'angularUtils.directives.dirPagination',
    'angularjs-dropdown-multiselect',
    'daterangepicker',
    'ui.bootstrap.datetimepicker',
    'smoothScroll'
    ]);

app
  .config(['localStorageServiceProvider', function(localStorageServiceProvider){
    localStorageServiceProvider.setPrefix('ls');
  }])
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('main', {
        url:'/',
        views: {
          // main home page
          'main@': { 
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
          },

          // footer
          'footer': { templateUrl: 'views/footer.html' }
        },
        data : {
          requireLogin : false
        }
      })
      .state('timer', {
        url:'/timer',
        views: {
          // main timer page
          'main': { 
            templateUrl: 'views/timer-page.html',
            controller: 'DateTimePickerCtrl' 
          },

          // footer
          'footer': { templateUrl: 'views/footer.html' }
        },
        data : {
          requireLogin : true
        }
      })
      .state('report', {
        url:'/report',
        views: {
          // main report page
          'main': { 
            templateUrl: 'views/report.html',
            controller: 'ReportCtrl' 
          },

          // footer
          'footer': { templateUrl: 'views/footer.html' }
        },
        data : {
          requireLogin : true
        }
      })
      .state('project', {
        url:'/project',
        views: {
          // main project page
          'main': { 
            templateUrl: 'views/project.html',
            controller: 'ProjectPageCtrl'
          },

          // footer
          'footer': { templateUrl: 'views/footer.html' }
        },
        data : {
          requireLogin : true
        }
      })
      .state('new-project', {
        url:'/new-project',
        views: {
          // new project page
          'main': { 
            templateUrl: 'views/new-project.html',
            controller: 'NewProjectCtrl'
          },

          // footer
          'footer': { templateUrl: 'views/footer.html' }
        },
        data : {
          requireLogin : true
        }
      })
      .state('edit-project', {
        url:'/edit-project',
        views: {
          // edit project page
          'main': { 
            templateUrl: 'views/edit-project.html',
            controller: 'EditProjectCtrl' 
          },

          // footer
          'footer': { templateUrl: 'views/footer.html' }
        },
        data : {
          requireLogin : true
        }
      })
      .state('company', {
        url:'/company',
        views: {
          // edit project page
          'main': { 
            templateUrl: 'views/company.html',
            controller: 'CompanyCtrl' 
          },

          // footer
          'footer': { templateUrl: 'views/footer.html' }
        },
        data : {
          requireLogin : true
        }
      })
      .state('team', {
        url:'/team',
        views: {
          // edit project page
          'main': { 
            templateUrl: 'views/team.html',
            controller: 'TeamCtrl' 
          },

          // footer
          'footer': { templateUrl: 'views/footer.html' }
        },
        data : {
          requireLogin : true
        }
      })
      .state('manage-user', {
        url:'/manage-user',
        views: {
          // edit project page
          'main': { 
            templateUrl: 'views/manage-user.html',
            controller: 'ManageUserCtrl' 
          },

          // footer
          'footer': { templateUrl: 'views/footer.html' }
        },
        data : {
          requireLogin : true
        }
      })
      .state('feedback', {
        url:'/feedback',
        views: {
          // edit project page
          'main': { 
            templateUrl: 'views/feedback.html',
            controller: 'FeedbackCtrl' 
          },

          // footer
          'footer': { templateUrl: 'views/footer.html' }
        },
        data : {
          requireLogin : true
        }
      })
      .state('support', {
        url:'/support',
        views: {
          // edit project page
          'main': { 
            templateUrl: 'views/support.html',
            controller: 'SupportCtrl' 
          },

          // footer
          'footer': { templateUrl: 'views/footer.html' }
        },
        data : {
          requireLogin : false
        }
      });
  });