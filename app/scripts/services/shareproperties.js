'use strict';

/**
 * @ngdoc service
 * @name timeTravelerFrontApp.shareProperties
 * @description
 * # shareProperties
 * Service in the timeTravelerFrontApp.
 */
var app = angular.module('timeTravelerFrontApp');

app.service('shareProperties', function () {
    var property;
    return {
        getProperty: function () {
            return property;
        },
        setProperty: function(value) {
            property = value;
        }
    };
});
