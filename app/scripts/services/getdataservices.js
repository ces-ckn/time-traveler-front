'use strict';

/**
 * @ngdoc service
 * @name timeTravelerFrontApp.getDataServices
 * @description
 * # getDataServices
 * Service in the timeTravelerFrontApp.
 */
var app = angular.module('timeTravelerFrontApp');
  
app.service('getDataServices', ['$http', '$q', function($http, $q) {
    
    this.getAllData = function(apiUrl, destination, sendingData) {
        var deferred = $q.defer();
        return $http({
            method: 'POST',
            cache: false,
            url: apiUrl + destination,
            data: $.param(sendingData),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .then(function(response) {
            // promise is fulfilled
            deferred.resolve(response.data);
            // promise is returned
            return deferred.promise;
        }, function(response) {
            // the following line rejects the promise 
            deferred.reject(response);
            // promise is returned
            return deferred.promise;
        });
    };
}]);
