'use strict';

/**
 * @ngdoc directive
 * @name timeTravelerFrontApp.directive:logout
 * @description
 * # logout
 */
angular.module('timeTravelerFrontApp')
  .directive('logout', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
      	console.log("directives/logout 1");
        $(element).on('click', function() {
        	console.log("directives/logout 2");
        });
      }
    };
  });
