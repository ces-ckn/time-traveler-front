'use strict';

/**
 * @ngdoc directive
 * @name timeTravelerFrontApp.directive:menu
 * @description
 * # menu
 */
angular.module('timeTravelerFrontApp')
  .directive('menu', function () {
    return {
      restrict: 'E',
      template: '<div class="nav-bar">
    	<div id="sidebar-wrapper">
			<ul class="sidebar-nav">
		        <li>
		            <a href="#/">Home</a>
		        </li>
		        <li>
		            <a href="#/#introduce">Introducing</a>
		        </li>
		        <li>
		            <a href="#/#pricing">Pricing</a>
		        </li>
		        <li>
		            <a href="#/#about">About Us</a>
		        </li>
		        <li>
		            <a href="#/#contact">Contact Us</a>
		        </li>
		    </ul>
		</div>
	</div>'
    };
  });
