'use strict';

describe('Controller: DateTimePickerCtrl', function () {

  // load the controller's module
  beforeEach(module('timeTravelerFrontApp'));

  var DateTimePickerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DateTimePickerCtrl = $controller('DateTimePickerCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
