'use strict';

describe('Controller: NewProjectCtrl', function () {

  // load the controller's module
  beforeEach(module('timeTravelerFrontApp'));

  var NewProjectCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewProjectCtrl = $controller('NewProjectCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewProjectCtrl.awesomeThings.length).toBe(3);
  });
});
