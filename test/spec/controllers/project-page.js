'use strict';

describe('Controller: ProjectPageCtrl', function () {

  // load the controller's module
  beforeEach(module('timeTravelerFrontApp'));

  var ProjectPageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProjectPageCtrl = $controller('ProjectPageCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProjectPageCtrl.awesomeThings.length).toBe(3);
  });
});
