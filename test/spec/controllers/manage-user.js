'use strict';

describe('Controller: ManageUserCtrl', function () {

  // load the controller's module
  beforeEach(module('timeTravelerFrontApp'));

  var ManageUserCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ManageUserCtrl = $controller('ManageUserCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ManageUserCtrl.awesomeThings.length).toBe(3);
  });
});
