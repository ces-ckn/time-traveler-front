'use strict';

describe('Controller: ExtensionCtrl', function () {

  // load the controller's module
  beforeEach(module('timeTravelerFrontApp'));

  var ExtensionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExtensionCtrl = $controller('ExtensionCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
