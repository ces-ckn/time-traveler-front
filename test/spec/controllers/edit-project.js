'use strict';

describe('Controller: EditProjectCtrl', function () {

  // load the controller's module
  beforeEach(module('timeTravelerFrontApp'));

  var EditProjectCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EditProjectCtrl = $controller('EditProjectCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EditProjectCtrl.awesomeThings.length).toBe(3);
  });
});
