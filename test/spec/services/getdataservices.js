'use strict';

describe('Service: getDataServices', function () {

  // load the service's module
  beforeEach(module('timeTravelerFrontApp'));

  // instantiate service
  var getDataServices;
  beforeEach(inject(function (_getDataServices_) {
    getDataServices = _getDataServices_;
  }));

  it('should do something', function () {
    expect(!!getDataServices).toBe(true);
  });

});
