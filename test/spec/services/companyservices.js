'use strict';

describe('Service: companyServices', function () {

  // load the service's module
  beforeEach(module('timeTravelerFrontApp'));

  // instantiate service
  var companyServices;
  beforeEach(inject(function (_companyServices_) {
    companyServices = _companyServices_;
  }));

  it('should do something', function () {
    expect(!!companyServices).toBe(true);
  });

});
