'use strict';

describe('Service: shareProperties', function () {

  // load the service's module
  beforeEach(module('timeTravelerFrontApp'));

  // instantiate service
  var shareProperties;
  beforeEach(inject(function (_shareProperties_) {
    shareProperties = _shareProperties_;
  }));

  it('should do something', function () {
    expect(!!shareProperties).toBe(true);
  });

});
